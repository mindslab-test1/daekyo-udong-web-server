package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookAfterDetailMapper {
    List<BookAfter> selectBookafterList(BookAfter bookafter);
}
