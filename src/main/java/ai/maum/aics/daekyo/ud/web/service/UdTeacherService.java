package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookShelfDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.repository.*;
import ai.maum.aics.daekyo.ud.web.repository.entity.*;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class UdTeacherService {
    private ConfigProperties configProperties;
    private UdTeacherMapper udTeacherMapper;

    public void updateTeacheePw(UdTeacher udTeacher) {
        udTeacherMapper.updateTeacheePw(udTeacher);
    }

    public List<UdTeacher> getMonthIncomeByTeacherId(UdTeacher udTeacher) {

        List<UdTeacher> udTeacherList = udTeacherMapper.getMonthIncomeByTeacherId(udTeacher);
        List<UdTeacher> udTeacherVO = udTeacherMapper.getPaymentGroupByTypeAndDate(udTeacher);
//        List<UdTeacher.IncomeMonth> incomeMonthList = new ArrayList<>();
//        List<UdTeacher.Income> incomeList = new ArrayList<>();
//
//        Map<String, List<UdTeacher>> map = udTeacherList.stream().collect(Collectors.groupingBy(UdTeacher::getPaymentType));
//
//        map.forEach((key, value) -> {
//            UdTeacher.Income income = new UdTeacher.Income();
//            income.setPaymentType(key);
//            income.setUdTeacherList(value);
//            incomeList.add(income);
//        });

        for (int i = 0; i < udTeacherVO.size(); i++) {
            List<UdTeacher> addList = new ArrayList<>();
            for (UdTeacher a : udTeacherList) {
                if (a.getPaymentType().equals(udTeacherVO.get(i).getPaymentType()) && a.getScheduleDate().equals(udTeacherVO.get(i).getScheduleDate())) {
                    addList.add(a);
                }
            }
            udTeacherVO.get(i).setUdTeacherList(addList);
        }
        return udTeacherVO;
    }

    public String getPaymentMinDate(long teacherId) {
        return udTeacherMapper.getPaymentMinDate(teacherId);
    }

}
