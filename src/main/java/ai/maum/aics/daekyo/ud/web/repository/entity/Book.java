package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Book {
    private long userId;
    private long childId;
    private long bookId;
    private long id;
    private String title;
    private String publisher;
}
