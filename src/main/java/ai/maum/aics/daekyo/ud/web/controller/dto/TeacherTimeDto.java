package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherTime;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public class TeacherTimeDto {

    @Getter
    public static class Request {
        @Setter
        private long teacherId;
        private DayType dayType;

        public void setDayType(String dayType) {
            this.dayType = DayType.valueOf(dayType);
        }
    }

    @Getter
    @Setter
    public static class Response {
        private List<Integer> timeBlockList;
    }

    @Getter
    public static class Update {
        @Setter
        private long teacherId;
        private DayType dayType;
        @Setter
        private List<Integer> timeBlockList;

        public void setDayType(String dayType) {
            this.dayType = DayType.valueOf(dayType);
        }

        public static List<TeacherTime> toEntityList(TeacherTimeDto.Update update) {
            return update.getTimeBlockList().stream()
                    .map(integer -> {
                        TeacherTime teacherTime = new TeacherTime();
                        teacherTime.setTeacherId(update.getTeacherId());
                        teacherTime.setDayType(update.getDayType());
                        teacherTime.setTimeBlockIdx(integer);
                        return teacherTime;
                    }).collect(Collectors.toList());
        }
    }
}
