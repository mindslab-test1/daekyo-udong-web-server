package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.Data;

@Data
public class DkMediaResponseDto {

	private String fileName;
	private String fileDownloadUri;
	private String fileType;
	private int fileSize;

}
