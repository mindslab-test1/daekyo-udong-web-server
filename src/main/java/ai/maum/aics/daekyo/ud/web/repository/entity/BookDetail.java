package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class BookDetail {
    private long id;
    private String readDate;
    private boolean isInterest;
}
