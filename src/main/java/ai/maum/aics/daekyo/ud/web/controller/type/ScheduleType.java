package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ScheduleType {
    NONE,
    TEST,
    MATCHED,
    SCHEDULED,
    VISIT,
    VISIT_AFTER,
    CANCEL,
    NO_SHOW,
    COMPLETED;

    @Getter
    private final static List<ScheduleType> matchedScheduleTypeList = new ArrayList<>(
            Arrays.asList(MATCHED, SCHEDULED, VISIT)
    );
    @Getter
    private final static List<ScheduleType> completedScheduleTypeList = new ArrayList<>(
            Arrays.asList(VISIT_AFTER, COMPLETED)
    );
    @Getter
    private final static List<ScheduleType> endScheduleTypeList = new ArrayList<>(
            Arrays.asList(VISIT_AFTER, COMPLETED, CANCEL)
    );
}
