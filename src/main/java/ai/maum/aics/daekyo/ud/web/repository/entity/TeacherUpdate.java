package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class TeacherUpdate {

    private long teacherId;
    private String teacherTel;
    private String teacherContact;
    private String teacherAddress;
    private String teacherEmail;
    private String teacherImageUrl;
    private String teacherIntroduce;

    private String teacherOrgImage;
    private String teacherImage;
    private MultipartFile image;

}
