package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherTime;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherTimeMapper {
    List<TeacherTime> getList(long teacherId);
    List<Integer> getListByDayIdx(long teacherId, DayType dayType);
    void delete(long teacherId, DayType dayType);
    void insertList(List<TeacherTime> teacherTimeList);
}
