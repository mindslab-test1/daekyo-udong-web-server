package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.Book;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookMapper {
    List<Book> getBookList(long userId, long childId);
    List<BookDetail> getBookDetailList(long childId);
    List<BookAfter> getBookafterList();
}
