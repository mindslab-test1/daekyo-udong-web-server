package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

import java.util.List;

@Data
public class TimeBlockAvailable {
        private long timeBlockId;
        private boolean available = false;
}
