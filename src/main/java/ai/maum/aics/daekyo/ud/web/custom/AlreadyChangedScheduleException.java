package ai.maum.aics.daekyo.ud.web.custom;

public class AlreadyChangedScheduleException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public AlreadyChangedScheduleException(String message) {
        super("already change schedule exception", message);
        this.message = message;
    }


}
