package ai.maum.aics.daekyo.ud.web.repository;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherDateMapper {
    List<String> getList(long teacherId);
    List<String> getListByMonth(long teacherId, String yearMonth);
    void delete(long teacherId, String yearMonth);
    void insert(long teacherId, String unavailableDate);
}
