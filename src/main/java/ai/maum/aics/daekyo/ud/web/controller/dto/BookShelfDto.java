package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.repository.entity.Book;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookDetail;
import ai.maum.aics.daekyo.ud.web.util.ModelMapperUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BookShelfDto {

    @Getter
    @Setter
    public static class RequestList {
        private String query;
        private Boolean isRead;
    }

    @Getter
    @Setter
    public static class ResponseList {
        private String childName;
        private List<BookDto> bookList;

        public static ResponseList of(String childName, List<Book> bookList,
                                      List<BookDetail> bookDetailList, List<Long> bookcaseIdList) {
            ResponseList responseList = new ResponseList();
            responseList.setChildName(childName);

            Map<Long, ReadInfo> readInfoMap = bookDetailList.stream()
                    .collect(
                            Collectors.toMap(BookDetail::getId, ReadInfo::of)
                    );

            Map<Long, Long> bookAfterCntMap = bookcaseIdList.stream()
                    .collect(
                            Collectors.groupingBy(Long::longValue, Collectors.counting())
                    );

            List<BookDto> bookDtoList = bookList.stream()
                    .map(book -> {
                        BookDto bookDto = BookDto.of(book);
                        ReadInfo readInfo = readInfoMap.getOrDefault(book.getId(), null);
                        bookDto.setReadInfo(readInfo);
                        long bookAfterCnt = bookAfterCntMap.getOrDefault(book.getId(), 0L);
                        bookDto.setBookAfterCnt(bookAfterCnt);
                        return bookDto;
                    }).collect(Collectors.toList());

            responseList.setBookList(bookDtoList);
            return responseList;
        }
    }

    @Getter
    @Setter
    public static class BookDto {
        private long id;
        private long bookId;
        private long userId;
        private long childId;
        private String title;
        private String publisher;
        private ReadInfo readInfo;
        private long bookAfterCnt;

        public static BookDto of(Book book) {
            return ModelMapperUtils.getModelMapper().map(book, BookDto.class);
        }
    }

    @Getter
    @Setter
    public static class ReadInfo {
        private String readDate;
        private boolean isInterest;

        public static ReadInfo of(BookDetail bookDetail) {
            return ModelMapperUtils.getModelMapper().map(bookDetail, ReadInfo.class);
        }
    }
}
