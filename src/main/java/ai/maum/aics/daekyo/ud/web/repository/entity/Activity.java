package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Activity {
    private long serviceId;
    private String groupId;
    private long order;
    private String name;
}
