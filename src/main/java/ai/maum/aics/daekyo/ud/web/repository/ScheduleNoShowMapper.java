package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.NoShow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ScheduleNoShowMapper {
    int insert(long scheduleId);
    List<NoShow> getList(List<Long> scheduleIdList);
}
