//package ai.maum.aics.daekyo.ud.web.service;
//
//import ai.maum.aics.daekyo.ud.web.controller.dto.MailDto;
//import ai.maum.aics.daekyo.ud.web.util.MailHandler;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//
//import javax.mail.MessagingException;
//
//@Slf4j
//@Service
//@AllArgsConstructor
//public class MailService {
//    private JavaMailSender mailSender;
//
//    @Async
//    public void mailSend(MailDto mailDto) {
//        try {
//            MailHandler mailHandler = new MailHandler(mailSender);
//
//            mailHandler.setTo(mailDto.getToAddress());
//            mailHandler.setSubject(mailDto.getSubject());
//            mailHandler.setText(mailDto.getText(), true);
//            mailHandler.setInline("logo_daekyo_kids", "/static/images/logo_daekyo_kids.png");
//
//            mailHandler.send();
//        } catch (Exception e) {
//            log.error("fail to send mail", e);
//        }
//    }
//}
