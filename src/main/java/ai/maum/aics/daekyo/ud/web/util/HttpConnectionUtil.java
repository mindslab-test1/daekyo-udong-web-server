package ai.maum.aics.daekyo.ud.web.util;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.DkMediaResponseDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.MessageVO;
import org.apache.commons.io.FilenameUtils;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class HttpConnectionUtil {

    public int postRequest(BookafterDto bookafterDto, ConfigProperties configProperties) throws Exception {

        String bookafterRegist = configProperties.getApp() + configProperties.getBookafter();
        URL url = new URL(bookafterRegist);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("APP-KEY", configProperties.getKey());
        con.setRequestMethod("POST");
        con.setUseCaches( false );

        Map<String,Object> params = new LinkedHashMap<>();
        params.put("bookcaseId", bookafterDto.getBookcaseId());

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");


        OutputStream os = con.getOutputStream();
        os.write(params.toString().getBytes("UTF-8"));
        os.flush();
        os.close();

        int httpResult = con.getResponseCode();

        return httpResult;

    }

    public int messageService(String serverUrl, String messageEndPoint, MessageVO messageVO) throws Exception {

        String messageServer = serverUrl + messageEndPoint;
        URL url = new URL(messageServer);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("POST");

        JSONObject cred = new JSONObject();
        cred.put("scheduleId", messageVO.getScheduleId());
        cred.put("destPhone", messageVO.getDestPhone());
        cred.put("childName", messageVO.getChildName());
        cred.put("visitDate", messageVO.getVisitDate());
        cred.put("visitAddr", messageVO.getVisitAddr());
        cred.put("teacherName", messageVO.getTeacherName());
        cred.put("authNumber", messageVO.getAuthNumber());
        cred.put("typeCode", messageVO.getTypeCode());

        OutputStream os = con.getOutputStream();
        os.write(cred.toString().getBytes("UTF-8"));
        os.close();

        int httpResult = con.getResponseCode();

        return httpResult;

    }

    public String mediaServerSender(MultipartFile file, String serverUrl, String directory) throws Exception {

        ByteArrayResource fileResource = new ByteArrayResource(file.getBytes()) {
            // 기존 ByteArrayResource의 getFilename 메서드 override
            @Override
            public String getFilename() {
                return UUID.randomUUID().toString() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
            }
        };

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileResource);
        body.add("uploadDirectory", "/aics/daekyo");
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        DkMediaResponseDto dkMediaResponseDto = new DkMediaResponseDto();
        ResponseEntity<DkMediaResponseDto> response = restTemplate.exchange(serverUrl, HttpMethod.POST, requestEntity, DkMediaResponseDto.class);

        return response.getBody().getFileDownloadUri();
    }

    public int cancelRequest(long scheduleId, String cancelType, String messageServer) throws Exception {

        String inicisServerURL = messageServer + "/billingRefund";
        URL url = new URL(inicisServerURL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("POST");

        JSONObject cred = new JSONObject();
        cred.put("scheduleId",scheduleId);
        cred.put("typeCode", cancelType);

        OutputStream os = con.getOutputStream();
        os.write(cred.toString().getBytes("UTF-8"));
        os.close();

        int httpResult = con.getResponseCode();

        return httpResult;

    }

}