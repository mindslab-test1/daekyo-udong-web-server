package ai.maum.aics.daekyo.ud.web.custom;

public class TimeMissMatchException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public TimeMissMatchException(String message) {
        super("time miss match exception", message);
        this.message = message;
    }


}
