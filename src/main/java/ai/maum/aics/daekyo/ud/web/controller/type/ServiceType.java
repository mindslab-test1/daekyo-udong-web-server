package ai.maum.aics.daekyo.ud.web.controller.type;

public enum ServiceType {

    NONE,
    MATCHING,
    FAIL_MATCHING,
    FAIL_PAYMENT,
    CANCEL,
    MATCHED,
    REMATCHING,
    SUCCESS_PAYMENT,
}
