package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Addr {
    private long teacherId;
    private String addrBcode;
    private String addrSido;
    private String addrSigungu;
    private String addrBname1;
    private String addrBname2;
    private String addrAddress;
}
