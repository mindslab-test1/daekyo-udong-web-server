package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.UserDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.UdTeacher;
import ai.maum.aics.daekyo.ud.web.repository.entity.User;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.service.UdTeacherService;
import ai.maum.aics.daekyo.ud.web.service.UserService;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private UserService userService;
    private UdTeacherService udTeacherService;
    private TeacherDetailService teacherDetailService;

    @GetMapping("/login")
    public String login() {
        return "thymeleaf/login";
    }


    @PostMapping("/find_id")
    public @ResponseBody boolean findId(@RequestBody User user) {

        Long id = userService.findCompanyNoForEmail(user.getName(), user.getEmail());

        if (id == null) return false;

        userService.findId(user.getEmail(), id);

        return true;
    }

    @PostMapping("/forgot_password")
    public @ResponseBody boolean forgotPassword(@RequestBody User user) {

        Long id = userService.findDetailForEmail(user);

        if (id == null) return false;

        userService.forgotPassword(user);

        return true;
    }

    @GetMapping("/update_password")
    public String loginSetPassword(@ModelAttribute("user") UserDto.UpdatePassword user) {
//        todo reset token 검증
        return "thymeleaf/login_setpassword";
    }

    // HTML5 Form Tag => not support method (put, patch, delete)
    @PostMapping("/update_password")
    public String updatePassword(@Valid @ModelAttribute("user") UserDto.UpdatePassword user, BindingResult result, OAuth2Authentication oAuth2Authentication) {
        Long companyNo = null;
        if (oAuth2Authentication != null) {
            companyNo = Long.parseLong(OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication));
        }

        UdTeacher updatedUdTeacher = teacherDetailService.updateNewPassword(user, companyNo);

        if (updatedUdTeacher == null) {
            return "thymeleaf/login_setpassword";
        }

        udTeacherService.updateTeacheePw(updatedUdTeacher);

        return "redirect:/user/login";
    }
}
