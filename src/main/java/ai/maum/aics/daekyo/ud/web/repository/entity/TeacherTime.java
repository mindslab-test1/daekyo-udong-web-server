package ai.maum.aics.daekyo.ud.web.repository.entity;

import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import lombok.Data;

@Data
public class TeacherTime {
    private long teacherId;
    private DayType dayType;
    private int timeBlockIdx;
}
