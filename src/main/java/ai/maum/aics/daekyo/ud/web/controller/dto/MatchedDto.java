package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.controller.type.NoShowType;
import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import lombok.Getter;
import lombok.Setter;

public class MatchedDto {

    @Getter
    @Setter
    public static class ChangeSchedule {
        private long teacherId;
        private long scheduleId;
        private String scheduleDate;
        private int startTimeIdx;
        private int endTimeIdx;

        public void setEndTimeIdx(int timeDiff) {
            endTimeIdx = startTimeIdx + timeDiff;
        }

        public String getScheduleDate() {
            return scheduleDate.replaceAll("-", "");
        }

        public boolean overlapTime(int startTimeIdx, int endTimeIdx) {
            return (this.startTimeIdx <= endTimeIdx) && (startTimeIdx <= this.endTimeIdx);
        }
    }

    @Getter
    @Setter
    public static class Get {
        private ScheduleDto scheduleDto;
        private Long noShowId;

        public NoShowType getNoShowType() {

            if (noShowId != null) {
                return NoShowType.IN_PROGRESS;
            }

            ScheduleType test = ScheduleType.VISIT;
            if (scheduleDto.getScheduleType() == ScheduleType.VISIT && scheduleDto.getDateTime().getNoshowYn().equals("Y")) {
                return NoShowType.OPEN;
            }
            return NoShowType.CLOSE;
        }
    }
}
