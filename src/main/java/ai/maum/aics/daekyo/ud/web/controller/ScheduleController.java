package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDateDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherTimeDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.VisitTimetableDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.Teacher;
import ai.maum.aics.daekyo.ud.web.service.ScheduleService;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class ScheduleController {

    private ScheduleService scheduleService;
    private TeacherDetailService teacherDetailService;

    @GetMapping("/")
    public String main(OAuth2Authentication oAuth2Authentication) {
        if (oAuth2Authentication.getAuthorities() != null) {
            return "/visit_timetable";
        }
        return "thymeleaf/login";
    }

    @GetMapping("/exception")
    public String exception() {
        return "thymeleaf/exception";
    }

    @GetMapping("/visit_timetable")
    public String visitTimetable(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        model.addAttribute("visitTimetable", scheduleService.getVisitTimetable(teacherDetailDto.getId()));

        return "thymeleaf/visit_timetable";
    }

    @GetMapping("/set_schedule")
    public String setSchedule(Model model, OAuth2Authentication oAuth2Authentication) {
        //        todo 3~93을 설정에서 가져옴
        model.addAttribute("timeBlockList", scheduleService.getTimeBlockList());
        model.addAttribute("todayDayWeek", scheduleService.getTodayDayWeek());
        return "thymeleaf/set_schedule";
    }

    @GetMapping("/set_schedule/teacher_time_list")
    public @ResponseBody TeacherTimeDto.Response getTeacherTimeList(TeacherTimeDto.Request request, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        request.setTeacherId(teacherDetailDto.getId());
        return scheduleService.getTeacherTimeList(request);
    }

    @PostMapping("/set_schedule/teacher_time_list")
    public @ResponseBody boolean updateTeacherTimeList(@RequestBody TeacherTimeDto.Update update, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        update.setTeacherId(teacherDetailDto.getId());
        return scheduleService.updateTeacherTimeList(update);
    }

    @GetMapping("/set_schedule/teacher_date_list")
    public @ResponseBody
    TeacherDateDto.ResponseList getTeacherDateList(TeacherDateDto.RequestList requestList, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        requestList.setTeacherId(teacherDetailDto.getId());
        return scheduleService.getTeacherDateList(requestList);
    }

    @PostMapping("/set_schedule/teacher_date_list")
    public @ResponseBody boolean updateTeacherDateList(@RequestBody TeacherDateDto.Update update, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        update.setTeacherId(teacherDetailDto.getId());
        return scheduleService.updateTeacherDateList(update);
    }

    @GetMapping("/set_schedule/check_date")
    public @ResponseBody TeacherDateDto.ResponseCheck checkDate(TeacherDateDto.RequestCheck requestCheck, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        requestCheck.setTeacherId(teacherDetailDto.getId());

        return scheduleService.checkDate(requestCheck);
    }

    @PostMapping("/set_schedule/teacher_schedule")
    public @ResponseBody boolean teacherSchedule(OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        List<Long> scheduleList = scheduleService.getScheduleList(teacherDetailDto.getId());

        return scheduleList.size() == 0;

    }
    @PostMapping("/set_schedule/teacher_schedule_disabled")
    public @ResponseBody Teacher teacherScheduleDisabled(OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Teacher teacher = scheduleService.getTeacherScheduleAndRequestYn(teacherDetailDto.getId());

        return teacher;
    }

    @PostMapping("/set_schedule/teacher_reject")
    public @ResponseBody void teacherReject(@RequestBody String rejectYn, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Teacher teacher = new Teacher();
        teacher.setTeacherId(teacherDetailDto.getId());
        teacher.setRequestYn(rejectYn);
        scheduleService.updateRequestYn(teacher);
    }

    @PostMapping("/set_schedule/teacher_scheduleYn")
    public @ResponseBody String teacherScheduleYn(@RequestBody String scheduleYn, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Teacher teacher = new Teacher();
        teacher.setTeacherId(teacherDetailDto.getId());
        teacher.setScheduleYn(scheduleYn);
        scheduleService.updateScheduleYn(teacher);

        return scheduleYn;
    }
}
