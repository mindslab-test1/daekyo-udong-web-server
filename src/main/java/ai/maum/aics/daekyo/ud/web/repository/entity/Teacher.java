package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Teacher {
    private long id;
    private long teacherId;
    private long companyNo;
    private String name;
    private String email;
    private String requestYn;
    private String scheduleYn;
}
