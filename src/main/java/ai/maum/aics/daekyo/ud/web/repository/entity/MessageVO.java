package ai.maum.aics.daekyo.ud.web.repository.entity;


import lombok.Data;

@Data
public class MessageVO {

	private int scheduleId;
	private String destPhone;
	private String childName;
	private String visitDate;
	private String visitAddr;
	private String authNumber;
	private String teacherName;
	private String typeCode;

}
