package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.ServiceSchedule;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ServiceMapper {
    int accept(long teacherId, long serviceId);
    ServiceSchedule getSchedule(long serviceId);
}
