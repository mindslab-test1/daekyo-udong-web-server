package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.Addr;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherProfile;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherUpdate;
import ai.maum.aics.daekyo.ud.web.service.BookShelfService;
import ai.maum.aics.daekyo.ud.web.service.ScheduleService;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import ai.maum.aics.daekyo.ud.web.util.HttpConnectionUtil;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/profile")
@AllArgsConstructor
public class profileController {

    private TeacherDetailService teacherDetailService;
    private ScheduleService scheduleService;
    private BookShelfService bookShelfService;
    private ConfigProperties configProperties;

    @GetMapping("/myPage")
    public String view(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        long teacherId = teacherDetailDto.getId();

        model.addAttribute("teacherDetail", teacherDetailService.getTeacherDetail(teacherId));
        model.addAttribute("teacherSchool", teacherDetailService.getTeacherSchool(teacherId));
        model.addAttribute("teacherCertificateList", teacherDetailService.getTeacherCertificateList(teacherId));
        model.addAttribute("teacherCertificateForeignList", teacherDetailService.getTeacherForeignCertificateList(teacherId));
        model.addAttribute("teacherHopeAddrList", teacherDetailService.getTeacherHopeAddrList(teacherId));
        model.addAttribute("teacherActivityList", teacherDetailService.getTeacherActivityList(teacherId));
        model.addAttribute("teacherTimeBlockList", teacherDetailService.getTimeBlockList(teacherId));
        model.addAttribute("teacherCareerList", teacherDetailService.getCareer(teacherId));
        model.addAttribute("teacherVisitLogList", teacherDetailService.getvisitLog(teacherId));

        return "thymeleaf/profile";
    }

    @PostMapping("/activity")
    public @ResponseBody List<TeacherProfile> changeSchedule(OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        return teacherDetailService.getTeacherActivityList(teacherDetailDto.getId());
    }

    @PostMapping("/update/activity")
    public @ResponseBody boolean changeSchedule(@RequestBody String jsonData, OAuth2Authentication oAuth2Authentication) throws Exception {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        String name = URLDecoder.decode(jsonData, "UTF-8");
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(name.split("=")[1]);
        JSONArray jsonarray = (JSONArray)obj;
        jsonarray.add(obj);

        teacherDetailService.deleteActivityByTeacherId(teacherDetailDto.getId());

        for (int i = 0; i < jsonarray.size() - 1; i++) {
            TeacherProfile teacherProfile = new TeacherProfile();
            JSONObject jsonObject = (JSONObject)jsonarray.get(i);

            teacherProfile.setGrpId((String) jsonObject.get("grpId"));
            teacherProfile.setCdId((String) jsonObject.get("cdId"));
            teacherProfile.setTeacherId(teacherDetailDto.getId());

            teacherDetailService.insertActivity(teacherProfile);
        }

        return true;
    }

    @PostMapping("/cellPhone")
    public @ResponseBody void cellPhone(@RequestBody String phoneNumber, OAuth2Authentication oAuth2Authentication) throws Exception {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        teacherDetailDto.setTel(phoneNumber);

        new HttpConnectionUtil().messageService(configProperties.getMessage(), configProperties.getAuthAt(), teacherDetailService.cellPhoneMessage(teacherDetailDto));

    }

    @PostMapping("/checkAuth")
    public @ResponseBody boolean checkAuth(@RequestBody String authNo, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        TeacherProfile teacherProfile = new TeacherProfile();
        teacherProfile.setAuthNo(authNo);
        teacherProfile.setTeacherId(teacherDetailDto.getId());

        if (teacherDetailService.getPhoneAuthByTeacherId(teacherProfile) != null) {
            return true;
        } else {
            return false;
        }
    }

    @PostMapping("/emailModify")
    public @ResponseBody void emailModify(@RequestBody String email, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        teacherDetailService.mailModify(email, teacherDetailDto);

    }

    @GetMapping("/emailAlert")
    public String emailAlert(@RequestParam String id) {

        long mailAuthId = teacherDetailService.getMailAuthByTeacherId(Long.parseLong(id)).getMailAuthId();

        if (mailAuthId != 0) {
            teacherDetailService.updateMailAuthCompleteYn(mailAuthId);
        }

        return "thymeleaf/email_alert";

    }

    @PostMapping("/checkComplete")
    public @ResponseBody boolean checkComplete(OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        if (teacherDetailService.getMailAuthByComplete(teacherDetailDto.getId()) != null) {
            return true;
        }
        return false;
    }

    @PostMapping("/imageUpload")
    public @ResponseBody String imageUpload(TeacherUpdate teacherUpdate, OAuth2Authentication oAuth2Authentication) throws Exception {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        int index = teacherUpdate.getImage().getOriginalFilename().indexOf(".");
        teacherUpdate.setTeacherOrgImage(teacherUpdate.getImage().getOriginalFilename().substring(0, index));
        teacherUpdate.setTeacherImage(new HttpConnectionUtil().mediaServerSender(teacherUpdate.getImage(), configProperties.getUpload(), configProperties.getUploadUdong()));

        Gson gson = new Gson();
        String json = gson.toJson(teacherUpdate);

        return json;
    }

    @PostMapping("/updateDetail")
    public @ResponseBody boolean updateDetail(@RequestBody TeacherUpdate teacherUpdate, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        teacherUpdate.setTeacherId(teacherDetailDto.getId());

        teacherDetailService.updateTeacherDetail(teacherUpdate);

        return true;
    }

    @PostMapping("region")
    public @ResponseBody String region(@RequestBody Addr addr, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Gson gson = new Gson();
        String json = null;
        List<Addr> addrList = new ArrayList<>();

        if (addr.getAddrSido() == null) {
            addrList = teacherDetailService.getAddrSido();
            json = gson.toJson(addrList);
        } else if (addr.getAddrSigungu() == null) {
            addrList = teacherDetailService.getAddrSigungu(addr);
            json = gson.toJson(addrList);
        } else if (addr.getAddrBname1() == null) {
            addrList = teacherDetailService.getAddrBname1(addr);
            json = gson.toJson(addrList);
        } else if (addr.getAddrBname2() == null) {
            addrList = teacherDetailService.getAddrBname2(addr);
            json = gson.toJson(addrList);
        } else {
            json = gson.toJson(addrList);
        }
        return json;
    }

    @PostMapping("insertHopeAddr")
    public @ResponseBody void insertHopeAddr(@RequestBody Addr addr, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        String bCode = teacherDetailService.getBcode(addr);

        addr.setAddrBcode(bCode);
        addr.setTeacherId(teacherDetailDto.getId());

        teacherDetailService.insertHopeAddr(addr);
    }

    @PostMapping("deleteHopeAddr")
    public @ResponseBody void deleteHopeAddr(@RequestBody String addrBcode, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Addr addr = new Addr();
        addr.setAddrBcode(addrBcode);
        addr.setTeacherId(teacherDetailDto.getId());

        teacherDetailService.updateHopeAddrDelete(addr);
    }

    @PostMapping("/update/preferredAge")
    public @ResponseBody boolean preferredAge(@RequestBody String jsonData, OAuth2Authentication oAuth2Authentication) throws Exception {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        String name = URLDecoder.decode(jsonData, "UTF-8");
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(name.split("=")[1]);
        JSONArray jsonarray = (JSONArray)obj;
        jsonarray.add(obj);

        TeacherProfile teacherProfile = new TeacherProfile();
        teacherProfile.setTeacherId(teacherDetailDto.getId());

        for (int i = 0; i < jsonarray.size() - 1; i++) {
            JSONObject jsonObject = (JSONObject)jsonarray.get(i);
            if (jsonObject.get("age").equals("ageC")) {
                teacherProfile.setTeacherActiveC((String) jsonObject.get("yn"));
            }
            if (jsonObject.get("age").equals("ageT")) {
                teacherProfile.setTeacherActiveT((String) jsonObject.get("yn"));
            }
        }
        teacherDetailService.updateTeacherPreferredAge(teacherProfile);
        return true;
    }

}
