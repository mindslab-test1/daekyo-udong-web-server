package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.ApplyDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.CompletedDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.MatchedDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.ScheduleDto;
import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import ai.maum.aics.daekyo.ud.web.controller.type.ServiceType;
import ai.maum.aics.daekyo.ud.web.custom.*;
import ai.maum.aics.daekyo.ud.web.repository.*;
import ai.maum.aics.daekyo.ud.web.repository.entity.*;
import ai.maum.aics.daekyo.ud.web.util.DateHandler;
import ai.maum.aics.daekyo.ud.web.util.HttpConnectionUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ManagementService {
    private ApplyMapper applyMapper;
    private ActivityMapper activityMapper;
    private ServiceMapper serviceMapper;
    private ScheduleMapper scheduleMapper;
    private TimeBlockMapper timeBlockMapper;
    private ScheduleNoShowMapper scheduleNoShowMapper;
    private BookAfterMapper bookAfterMapper;
    private TeacherVisitLogMapper teacherVisitLogMapper;
    private TeacherReviewMapper teacherReviewMapper;

    private void joinActivity(List<ScheduleDto> scheduleDtoList) {
        List<Long> serviceIdList = scheduleDtoList.stream()
                .map(ScheduleDto::getServiceId)
                .collect(Collectors.toList());
        if (0 < serviceIdList.size()) {
            List<Activity> activityList = activityMapper.getList(serviceIdList);
            Map<Long, Map<String, List<String>>> activityGroupMap = activityList.stream()
                    .sorted(Comparator.comparing(Activity::getOrder))
                    .collect(
                            Collectors.groupingBy(
                                    Activity::getServiceId,
                                    Collectors.groupingBy(
                                            Activity::getGroupId,
                                            Collectors.mapping(Activity::getName, Collectors.toList())
                                    )
                            )
                    );
            scheduleDtoList.stream()
                    .filter(scheduleDto -> activityGroupMap.containsKey(scheduleDto.getServiceId()))
                    .forEach(scheduleDto -> {
                        Map<String, List<String>> activityMap = activityGroupMap.get(scheduleDto.getServiceId());
                        ScheduleDto.Child child = scheduleDto.getChild();
                        child.setActivityList(
                                activityMap.getOrDefault("CD011", null)
                        );
                        child.setTypeList(
                                activityMap.getOrDefault("CD012", null)
                        );
                    });
        }
    }

    @Transactional(readOnly = true)
    public List<ApplyDto> getApplyList(long teacherId) {
        List<ApplyDto> applyList = applyMapper.getApplyList(teacherId);
        List<ScheduleDto> scheduleDtoList = applyList.stream()
                .map(ApplyDto::getScheduleDto)
                .collect(Collectors.toList());
        joinActivity(scheduleDtoList);
        return applyList;
    }

    @Transactional(readOnly = true)
    public List<MatchedDto.Get> getMatchedList(long teacherId) {
        List<ScheduleDto> scheduleDtoList = scheduleMapper.getList(teacherId, ScheduleType.getMatchedScheduleTypeList());
        joinActivity(scheduleDtoList);

        List<Long> scheduleIdList = scheduleDtoList.stream()
                .map(ScheduleDto::getId)
                .collect(Collectors.toList());
        Map<Long, Long> noShowMap;
        if (0 < scheduleIdList.size()) {
            List<NoShow> noShowList = scheduleNoShowMapper.getList(scheduleIdList);
            noShowMap = noShowList.stream()
                    .collect(
                            Collectors.toMap(NoShow::getScheduleId, NoShow::getNoShowId)
                    );
        } else {
            noShowMap = new HashMap<>();
        }

        return scheduleDtoList.stream()
                .sorted()
                .map(scheduleDto -> {
                    MatchedDto.Get matched = new MatchedDto.Get();
                    matched.setScheduleDto(scheduleDto);
                    Long noShowId = noShowMap.getOrDefault(scheduleDto.getId(), null);
                    matched.setNoShowId(noShowId);
                    return matched;
                }).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<CompletedDto.Get> getCompletedList(long teacherId) {
        List<ScheduleDto> scheduleDtoList = scheduleMapper.getList(teacherId, ScheduleType.getEndScheduleTypeList());
        joinActivity(scheduleDtoList);

        List<Long> scheduleIdList = scheduleDtoList.stream()
                .map(ScheduleDto::getId)
                .collect(Collectors.toList());

        Map<Long, Long> bookAfterCntMap;
        List<Long> visitLogList;
        Map<Long, String> reviewMap;
        if (0 < scheduleIdList.size()) {
            List<Long> bookAfterCntData = bookAfterMapper.getScheduleIdList(scheduleIdList);
            bookAfterCntMap = bookAfterCntData.stream()
                    .collect(
                            Collectors.groupingBy(Long::longValue, Collectors.counting())
                    );

            visitLogList = teacherVisitLogMapper.getScheduleIdList(scheduleIdList);

            List<Review> reviewList = teacherReviewMapper.getList(scheduleIdList);
            reviewMap = reviewList.stream()
                    .collect(
                            Collectors.toMap(Review::getScheduleId, Review::getReviewContent)
                    );
        } else {
            bookAfterCntMap = new HashMap<>();
            visitLogList = new ArrayList<>();
            reviewMap = new HashMap<>();
        }

        return scheduleDtoList.stream()
                .sorted(Comparator.reverseOrder())
                .map(scheduleDto -> {
                    CompletedDto.Get completed = new CompletedDto.Get();
                    completed.setScheduleDto(scheduleDto);

                    Long bookAfterCnt = bookAfterCntMap.getOrDefault(scheduleDto.getId(), 0L);
                    completed.setBookAfterCnt(bookAfterCnt);

                    boolean hasVisitLog = visitLogList.contains(scheduleDto.getId());
                    completed.setHasVisitLog(hasVisitLog);

                    String review = reviewMap.getOrDefault(scheduleDto.getId(), null);
                    completed.setReview(review);
                    return completed;
                }).collect(Collectors.toList());
    }

    @Transactional
    public void match(long teacherId, long applyId) throws ParseException {
        Apply apply = applyMapper.get(applyId);
        if (apply.getTeacherId() != teacherId || apply.isMatching()) {
            throw new NotEqualOrAlreadyMatchedException("apply id : " + apply.getTeacherId() + "login id : " + teacherId + "apply matched : " + apply.isMatching());
        }
        long serviceId = apply.getServiceId();
        int acceptCnt = serviceMapper.accept(teacherId, serviceId);
        if (acceptCnt == 0) {
            throw new NotFoundException("service id : " + serviceId);
        }
        applyMapper.match(serviceId);

        ServiceSchedule serviceSchedule = serviceMapper.getSchedule(serviceId);

        String endDateString = serviceSchedule.getServiceEndDate();
        Calendar endDate = DateHandler.parse(endDateString);

        String startDateString = serviceSchedule.getServiceStartDate();
        Calendar scheduleDate = DateHandler.parse(startDateString);

        Schedule schedule = Schedule.of(serviceSchedule);
        while (scheduleDate.compareTo(endDate) <= 0) {
            String scheduleDateString = DateHandler.toString(scheduleDate);
            schedule.setScheduleDate(scheduleDateString);
            scheduleMapper.insert(schedule);
            scheduleDate.add(Calendar.DATE, 7);
        }
        rejectApply(
                teacherId,
                serviceSchedule.getServiceStartDate(),
                serviceSchedule.getServiceEndDate(),
                serviceSchedule.getTimeBlockIdFrom(),
                serviceSchedule.getTimeBlockIdTo()
        );
    }

    @Transactional
    public void changeSchedule(MatchedDto.ChangeSchedule changeSchedule) {
        Schedule targetSchedule = validate(
                changeSchedule.getTeacherId(), changeSchedule.getScheduleId(), ScheduleType.MATCHED);
        if (targetSchedule.isChange()) {
            throw new AlreadyChangedScheduleException("already change schedule");
        }

        List<TimeBlock> timeBlockList = timeBlockMapper.getTimeBlockList();
        IntSummaryStatistics intSummaryStatistics = timeBlockList.stream()
                .mapToInt(TimeBlock::getId)
                .summaryStatistics();

        int timeDiff = targetSchedule.getTimeBlockIdTo() - targetSchedule.getTimeBlockIdFrom();
        changeSchedule.setEndTimeIdx(timeDiff);
        int timeBlockIdxMax = intSummaryStatistics.getMax();
        int timeBlockIdxMin =intSummaryStatistics.getMin();
        if (changeSchedule.getStartTimeIdx() < timeBlockIdxMin || timeBlockIdxMax < changeSchedule.getStartTimeIdx()) {
            throw new TimeMissMatchException("Already scheduled time start time id : " + changeSchedule.getStartTimeIdx() + "min time id : " + timeBlockIdxMin + " max time id : " + timeBlockIdxMax);
        }
        if (changeSchedule.getEndTimeIdx() < timeBlockIdxMin || timeBlockIdxMax < changeSchedule.getEndTimeIdx()) {
            throw new TimeMissMatchException("Already scheduled time end time id : " + changeSchedule.getStartTimeIdx() + "min time id : " + timeBlockIdxMin + " max time id : " + timeBlockIdxMax);
        }
        List<Schedule> scheduleList = scheduleMapper.getListByDate(
                changeSchedule.getTeacherId(), changeSchedule.getScheduleDate());
        for (Schedule schedule : scheduleList) {
            if (schedule.getScheduleId() != targetSchedule.getScheduleId() && changeSchedule.overlapTime(schedule.getTimeBlockIdFrom() - 1, schedule.getTimeBlockIdTo() + 1)) {
                throw new NotEqualOrAlreadyMatchedException("schedule id : " + schedule.getScheduleId() + "target schedule id " + targetSchedule.getScheduleId() + "teacher already match start time id : " + schedule.getTimeBlockIdFrom() + "end time id : " + schedule.getTimeBlockIdTo());
            }
        }

        List<Schedule> childScheduleList = scheduleMapper.getListByChildId(changeSchedule.getScheduleId(), changeSchedule.getScheduleDate());

        for (Schedule schedule : childScheduleList) {
            if (schedule.getScheduleId() != targetSchedule.getScheduleId() && changeSchedule.overlapTime(schedule.getTimeBlockIdFrom(), schedule.getTimeBlockIdTo())) {
                throw new NotEqualOrAlreadyMatchedException("schedule id : " + schedule.getScheduleId() + "target schedule id " + targetSchedule.getScheduleId() + "child already match start time id : " + schedule.getTimeBlockIdFrom() + "end time id : " + schedule.getTimeBlockIdTo());
            }
        }

        if (scheduleMapper.getUnavailableDate(changeSchedule.getTeacherId(), changeSchedule.getScheduleDate()) == 1) {
            throw new NotFoundException("unavailable date : " + changeSchedule.getScheduleDate());
        }



        targetSchedule.setScheduleDate(changeSchedule.getScheduleDate());
        targetSchedule.setTimeBlockIdFrom(changeSchedule.getStartTimeIdx());
        targetSchedule.setTimeBlockIdTo(changeSchedule.getEndTimeIdx());

        scheduleMapper.update(targetSchedule);

        rejectApply(changeSchedule.getTeacherId(),
                changeSchedule.getScheduleDate(), changeSchedule.getScheduleDate(),
                changeSchedule.getStartTimeIdx(), changeSchedule.getEndTimeIdx()
        );
    }

    @Transactional
    public void noShow(long teacherId, long scheduleId) {
        validate(teacherId, scheduleId, ScheduleType.VISIT);
        scheduleNoShowMapper.insert(scheduleId);
    }

    @Transactional
    public void confirmVisit(long teacherId, long scheduleId) {
        validate(teacherId, scheduleId, ScheduleType.VISIT_AFTER);
        scheduleMapper.complete(scheduleId);
    }

    @Transactional
    public void writeVisitLog(CompletedDto.WriteVisitLog writeVisitLog) {
        validate(writeVisitLog.getTeacherId(), writeVisitLog.getScheduleId(), ScheduleType.COMPLETED);

        List<Long> scheduleIdList = Collections.singletonList(writeVisitLog.getScheduleId());
        List<Long> bookAfterCntData = bookAfterMapper.getScheduleIdList(scheduleIdList);
        if (bookAfterCntData.size() == 0) {
            throw new NotFoundException("Not found bookAfter by schedule id list : " + bookAfterCntData);
        }
        teacherVisitLogMapper.insert(writeVisitLog.getScheduleId(), writeVisitLog.getContent());
    }

    @Transactional
    public void teacherCancel(CompletedDto.TeacherCancel teacherCancel, String messageServer) {

        ServiceSchedule serviceSchedule = scheduleMapper.getCancelTypeByScheduleId(teacherCancel.getScheduleId());

        if (serviceSchedule.getServiceState() == ServiceType.SUCCESS_PAYMENT.ordinal()) {
            HttpConnectionUtil httpConnection = new HttpConnectionUtil();

            try {
                httpConnection.cancelRequest(teacherCancel.getScheduleId(), serviceSchedule.getCancelType(), messageServer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        scheduleMapper.updateCancel(teacherCancel.getScheduleId());
        scheduleMapper.insertCancelContent(teacherCancel.getScheduleId(), teacherCancel.getContent());
    }

    private Schedule validate(long teacherId, long scheduleId, ScheduleType scheduleType) {
        Schedule schedule = scheduleMapper.get(scheduleId);
        if (schedule.getScheduleType() != scheduleType) {
            throw new NotEqualException("enum value schedule type : " + schedule.getScheduleType() + "validate type : " + scheduleType);
        }
        long serviceId = schedule.getServiceId();
        ServiceSchedule serviceSchedule = serviceMapper.getSchedule(serviceId);
        if (serviceSchedule.getTeacherId() != teacherId) {
            throw new NotEqualException("teacher id value schedule teacher id : " + serviceSchedule.getTeacherId() + "login teacher id : " + teacherId);
        }
        return schedule;
    }

    private void rejectApply(long teacherId, String startDate, String endDate, int timeBlockIdFrom, int timeBlockIdTo) {
        List<ApplyDto> applyDtoList = applyMapper.getApplyList(teacherId);
        for (ApplyDto applyDto : applyDtoList) {
            boolean isOverlap = applyDto.overlap(startDate, endDate, timeBlockIdFrom, timeBlockIdTo);
            if (isOverlap) {
                applyMapper.reject(applyDto.getId());
            }
        }
    }
}
