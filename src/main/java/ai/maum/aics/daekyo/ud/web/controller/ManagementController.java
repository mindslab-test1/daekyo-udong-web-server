package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.CompletedDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.MatchedDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.controller.type.ChangeErrorType;
import ai.maum.aics.daekyo.ud.web.repository.entity.Alarm;
import ai.maum.aics.daekyo.ud.web.repository.entity.Apply;
import ai.maum.aics.daekyo.ud.web.repository.entity.MessageVO;
import ai.maum.aics.daekyo.ud.web.service.ManagementService;
import ai.maum.aics.daekyo.ud.web.service.ScheduleService;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import ai.maum.aics.daekyo.ud.web.util.HttpConnectionUtil;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/management")
@AllArgsConstructor
public class ManagementController {

    private ScheduleService scheduleService;
    private ManagementService managementService;
    private TeacherDetailService teacherDetailService;
    private ConfigProperties configProperties;

    @GetMapping("/apply")
    public String apply(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        model.addAttribute("applyList", managementService.getApplyList(teacherDetailDto.getId()));

        return "thymeleaf/visit_management";
    }

    @PostMapping("/apply")
    public @ResponseBody boolean acceptApplication(@RequestBody Apply apply, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            managementService.match(teacherDetailDto.getId(), apply.getApplyId());

            Alarm alarm = scheduleService.getDetailForAlarm(apply.getServiceId());

            MessageVO messageVO = new MessageVO();
            messageVO.setDestPhone(alarm.getPhoneNumber());
            messageVO.setChildName(alarm.getRealName());
            messageVO.setTeacherName(alarm.getTeacherName());

            new HttpConnectionUtil().messageService(configProperties.getMessage(), "/udongMatchingComplete", messageVO);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @GetMapping("/matched")
    public String matched(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        model.addAttribute("timeBlockList", scheduleService.getTimeBlockList());
        model.addAttribute("matchedList", managementService.getMatchedList(teacherDetailDto.getId()));

        return "thymeleaf/visit_management";
    }

    @PostMapping("/change_schedule")
    public @ResponseBody ChangeErrorType changeSchedule(@RequestBody MatchedDto.ChangeSchedule changeSchedule, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            changeSchedule.setTeacherId(teacherDetailDto.getId());
            managementService.changeSchedule(changeSchedule);

            Alarm alarm = scheduleService.getDetailForAlarmByScheduleId(changeSchedule.getScheduleId());

            MessageVO messageVO = new MessageVO();
            messageVO.setDestPhone(alarm.getPhoneNumber());
            messageVO.setChildName(alarm.getRealName());
            messageVO.setVisitAddr(alarm.getAddrAddress());
            messageVO.setVisitDate(alarm.getScheduleTime());
            new HttpConnectionUtil().messageService(configProperties.getMessage(), "/udongChangeScheduleComplete", messageVO);

            return ChangeErrorType.NONE;
        } catch (IllegalStateException e) {
            return ChangeErrorType.CHILD;
        } catch (Exception e) {
            return ChangeErrorType.TEACHER;
        }
    }

    @PostMapping("/no_show")
    public @ResponseBody boolean noShow(@RequestBody Long scheduleId, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            managementService.noShow(teacherDetailDto.getId(), scheduleId);

            Alarm alarm = scheduleService.getDetailForAlarm(scheduleId);

            MessageVO messageVO = new MessageVO();
            messageVO.setDestPhone(alarm.getPhoneNumber());
            messageVO.setChildName(alarm.getRealName());
            new HttpConnectionUtil().messageService(configProperties.getMessage(), "/udongNoShowFromTeacher", messageVO);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @GetMapping("/completed")
    public String completed(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        model.addAttribute("completedList", managementService.getCompletedList(teacherDetailDto.getId()));
        return "thymeleaf/visit_management";
    }

    @PostMapping("/confirm_visit")
    public @ResponseBody boolean confirmVisit(@RequestBody Long scheduleId, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            managementService.confirmVisit(teacherDetailDto.getId(), scheduleId);

            Alarm alarm = scheduleService.getDetailForAlarmByScheduleId(scheduleId);

            MessageVO messageVO = new MessageVO();
            messageVO.setDestPhone(alarm.getPhoneNumber());
            messageVO.setChildName(alarm.getRealName());
            messageVO.setTeacherName(alarm.getTeacherName());
            new HttpConnectionUtil().messageService(configProperties.getMessage(), "/udongScheduleEnd", messageVO);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @PostMapping("/write_visit_log")
    public @ResponseBody boolean writeVisitLog(@RequestBody CompletedDto.WriteVisitLog writeVisitLog, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            writeVisitLog.setTeacherId(teacherDetailDto.getId());
            managementService.writeVisitLog(writeVisitLog);

            Alarm alarm = scheduleService.getDetailForAlarm(writeVisitLog.getScheduleId());

            MessageVO messageVO = new MessageVO();
            messageVO.setDestPhone(alarm.getPhoneNumber());
            messageVO.setChildName(alarm.getRealName());
            messageVO.setTeacherName(alarm.getTeacherName());
            new HttpConnectionUtil().messageService(configProperties.getMessage(), "/udongVisitLog", messageVO);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @PostMapping("/teacher_cancel")
    public @ResponseBody boolean teacherCancel(@RequestBody CompletedDto.TeacherCancel teacherCancel, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        try {
            teacherCancel.setTeacherId(teacherDetailDto.getId());
            managementService.teacherCancel(teacherCancel, configProperties.getMessage());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
