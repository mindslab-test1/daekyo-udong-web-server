package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmailDto {
    private String subject;
    private String to;
    private String text;
    private String from;

    public EmailDto(String subject, String to, String text, String from) {
        this.subject = subject;
        this.to = to;
        this.text = text;
        this.from = from;
    }
}
