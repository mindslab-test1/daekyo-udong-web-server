package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Apply {
    private long id;
    private long applyId;
    private long serviceId;
    private long teacherId;
    private boolean isMatching;
}
