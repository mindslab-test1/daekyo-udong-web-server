package ai.maum.aics.daekyo.ud.web.custom;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {
    private final String code;
    private final String message;

    public BaseException(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
