package ai.maum.aics.daekyo.ud.web.custom;

public class NotEqualOrAlreadyMatchedException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public NotEqualOrAlreadyMatchedException(String message) {
        super("not equal or already matched exception", message);
        this.message = message;
    }


}
