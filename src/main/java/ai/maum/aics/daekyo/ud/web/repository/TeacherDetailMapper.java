package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeacherDetailMapper {
    public TeacherDetailDto selectTeacherByEmail(String email);
    public TeacherDetailDto selectTeacherByCompanyNo(Long companyNo);
}
