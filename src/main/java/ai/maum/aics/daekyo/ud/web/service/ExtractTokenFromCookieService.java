package ai.maum.aics.daekyo.ud.web.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

@Component
public class ExtractTokenFromCookieService implements TokenExtractor {
    private static final Logger logger = LoggerFactory.getLogger(ExtractTokenFromCookieService.class);

    @Override
    public Authentication extract(HttpServletRequest httpServletRequest) {
        if (httpServletRequest.getCookies() == null || httpServletRequest.getCookies().length == 0) {
            return null;
        }

        Optional<Cookie> accessTokenCookieOptional = Arrays.stream(httpServletRequest.getCookies())
                .filter(cookie -> cookie.getName().equalsIgnoreCase("access_token"))
                .findFirst();

        if (accessTokenCookieOptional.isPresent()) {
            Cookie accessTokenCookie = accessTokenCookieOptional.get();
            return new PreAuthenticatedAuthenticationToken(accessTokenCookie.getValue(), "");
        } else {
            return null;
        }
    }
}
