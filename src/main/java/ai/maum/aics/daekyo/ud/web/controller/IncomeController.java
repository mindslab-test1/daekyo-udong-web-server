package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.UdTeacher;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.service.UdTeacherService;
import ai.maum.aics.daekyo.ud.web.util.DateUtils;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/income")
@AllArgsConstructor
public class IncomeController {

    private UdTeacherService udTeacherService;
    private TeacherDetailService teacherDetailService;

    @GetMapping("")
    public String getMonthList(Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        System.out.println(teacherDetailDto.getId());
        System.out.println(udTeacherService.getPaymentMinDate(teacherDetailDto.getId()));

        model.addAttribute("minDate", udTeacherService.getPaymentMinDate(teacherDetailDto.getId()));

        return "thymeleaf/income_details";
    }

    @PostMapping("")
    public @ResponseBody List<UdTeacher> postMonthList(@RequestBody UdTeacher udTeacher, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        udTeacher.setTeacherId(teacherDetailDto.getId());

        return udTeacherService.getMonthIncomeByTeacherId(udTeacher);
    }
}
