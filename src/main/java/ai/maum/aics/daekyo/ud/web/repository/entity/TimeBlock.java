package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class TimeBlock {
    private int id;
    private String startTime;
    private String endTime;
}
