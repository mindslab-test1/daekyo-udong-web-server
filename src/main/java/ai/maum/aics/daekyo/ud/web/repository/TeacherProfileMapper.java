package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.Addr;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherProfile;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherUpdate;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherProfileMapper {

    public TeacherProfile getTeacherDetail(long teacherId);
    public TeacherProfile getTeacherSchool(long teacherId);
    public List<TeacherProfile> getTeacherCertificateList(long teacherId);
    public List<TeacherProfile> getTeacherForeignCertificateList(long teacherId);
    public List<TeacherProfile> getTeacherHopeAddrList(long teacherId);
    public List<TeacherProfile> getTeacherActivityList(long teacherId);
    public void insertActivity(TeacherProfile teacherProfile);
    public void deleteActivityByTeacherId(long teacherId);
    public List<TeacherProfile> getTimeBlockByTeacherId(long teacherId);
    public List<TeacherProfile> getCareer(long teacherId);
    public void insertAuthByTeacherId(TeacherProfile teacherProfile);
    public TeacherProfile getPhoneAuthByTeacherId(TeacherProfile teacherProfile);
    public void registMailAuth(TeacherProfile teacherProfile);
    public TeacherProfile getMailAuthByTeacherId(long teacherId);
    public TeacherProfile getMailAuthByComplete(long teacherId);
    public void updateMailAuthCompleteYn(long mailAuthId);
    public List<Addr> getAddrSido();
    public List<Addr> getAddrSigungu(Addr addr);
    public List<Addr> getAddrBname1(Addr addr);
    public List<Addr> getAddrBname2(Addr addr);
    public void updateTeacherDetail(TeacherUpdate teacherUpdate);
    public String getBcode(Addr addr);
    public void insertHopeAddr(Addr addr);
    public void updateHopeAddrDelete(Addr addr);
    public List<TeacherProfile> getvisitLog(long teacherId);
    public void updateTeacherPreferredAge(TeacherProfile teacherProfile);
}
