package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum DayType {
    NONE("", ""),
    SUN("일", "sunday"),
    MON("월", ""),
    TUE("화", ""),
    WED("수", ""),
    THU("목", ""),
    FRI("금", ""),
    SAT("토", "saturday");

    private String korean;
    private String cls;
    @Getter
    private static DayType[] list = Arrays.copyOfRange(values(), 1, values().length);
}
