package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.controller.type.*;
import ai.maum.aics.daekyo.ud.web.custom.TypeMissMatchException;
import ai.maum.aics.daekyo.ud.web.util.DateHandler;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class ScheduleDto implements Comparable<ScheduleDto> {
    private long id;
    private long serviceId;
    private ScheduleType scheduleType;
    private VisitType visitType;
    private boolean isChange;

    private Child child;
    private DateTime dateTime;
    private Address address;
    private String attention;

    public boolean getIsChange() {
        return isChange || scheduleType != ScheduleType.MATCHED;
    }

    public String getDate() throws ParseException {
        return DateTime.parseDate(dateTime.getDate());
    }

    @Override
    public int compareTo(ScheduleDto other) {
        int dateCompare = dateTime.getDate().compareTo(other.getDateTime().getDate());
        if (dateCompare == 0) {
            return dateTime.getStartTime().compareTo(other.getDateTime().getStartTime());
        } else {
            return dateCompare;
        }
    }

    @Getter
    public static class Child {
        private static Pattern phoneNumberPattern = Pattern.compile("^(\\d{3})(\\d{3,4})(\\d{4})$");
        private String name;
        private String birthDay;
        private GenderType genderType;
        private String parentPhone;
        @Setter
        private List<String> activityList;
        @Setter
        private List<String> typeList;

        public String getBirthYear() {
            return birthDay.substring(2, 4);
        }

        public String getParentPhone() {
            Matcher phoneNumberMatcher = phoneNumberPattern.matcher(parentPhone);
            if (phoneNumberMatcher.matches()) {
                return String.format(
                        "%s-%s-%s",
                        phoneNumberMatcher.group(1),
                        phoneNumberMatcher.group(2),
                        phoneNumberMatcher.group(3)
                );
            } else {
                throw new TypeMissMatchException("phone number : " + phoneNumberMatcher.group(1) + phoneNumberMatcher.group(2) + phoneNumberMatcher.group(3));
            }
        }
    }

    @Getter
    public static class Address {
        private String main;
        private String detail;
    }

    @Getter
    public static class DateTime {
        private String date;
        private int startTimeIdx;
        private String startTime;
        private int endTimeIdx;
        private String endTime;
        private int classTime;
        private String noshowYn;

        public static String parseDate(String dateString) throws ParseException {
            Calendar calendar = DateHandler.parse(dateString);

            return String.format("%d.%d.%d",
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)
            );
        }

        public DayType getDayType() throws ParseException {
            Calendar calendar = DateHandler.parse(this.date);
            return DayType.values()[calendar.get(Calendar.DAY_OF_WEEK)];
        }

        public boolean overlap(int startTimeIdx, int endTimeIdx) {
            return (this.startTimeIdx <= endTimeIdx + 1) && (startTimeIdx <= this.endTimeIdx + 1);
        }
    }
}
