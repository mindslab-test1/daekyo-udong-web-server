package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class ServiceSchedule {
    private long teacherId;
    private long serviceId;
    private long scheduleId;
    private long userId;
    private long childId;
    private String serviceStartDate;
    private String serviceEndDate;
    private int scheduleClassTime;
    private int timeBlockIdFrom;
    private int timeBlockIdTo;
    private int serviceState;
    private int scheduleState;
    private String cancelType;
}
