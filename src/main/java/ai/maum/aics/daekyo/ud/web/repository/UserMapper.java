package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Optional;

@Mapper
public interface UserMapper {
    Optional<Long> findCompanyNo(String name, String email);
    Long findCompanyNoForEmail(String name, String email);
    Long findDetailForEmail(String name, String email, String code);
}
