package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.Getter;

import java.text.ParseException;

@Getter
public class ApplyDto {
    private long id;
    private String applyDate;
    private String endDate;
    private ScheduleDto scheduleDto;

    public String getStartDate() throws ParseException {
        return ScheduleDto.DateTime.parseDate(scheduleDto.getDateTime().getDate());
    }

    public String getEndDate() throws ParseException {
        return ScheduleDto.DateTime.parseDate(endDate);
    }

    public long getServiceId() {
        return scheduleDto.getServiceId();
    }

    public boolean overlap(String startDate, String endDate, int startTime, int endTime) {
        ScheduleDto.DateTime dateTime = scheduleDto.getDateTime();
        String thisStartDate = dateTime.getDate();
        boolean isDateOverlap = (thisStartDate.compareTo(endDate) <= 0) && (startDate.compareTo(this.endDate) <= 0);
        boolean isTimeOverlap = dateTime.overlap(startTime, endTime);
        return isDateOverlap && isTimeOverlap;
    }
}
