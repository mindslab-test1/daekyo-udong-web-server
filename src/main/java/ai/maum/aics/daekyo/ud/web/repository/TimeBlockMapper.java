package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.repository.entity.TimeBlock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TimeBlockMapper {
    List<TimeBlock> getTimeBlockList();
}
