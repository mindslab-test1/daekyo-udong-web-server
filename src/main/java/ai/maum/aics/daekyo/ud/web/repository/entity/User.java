package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class User {

    private String email;
    private String name;
    private String code;

}
