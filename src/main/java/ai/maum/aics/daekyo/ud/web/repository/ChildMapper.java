package ai.maum.aics.daekyo.ud.web.repository;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChildMapper {
    String getName(long childId);
}
