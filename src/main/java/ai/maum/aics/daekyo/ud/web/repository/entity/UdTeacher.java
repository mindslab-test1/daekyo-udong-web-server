package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
public class UdTeacher {

    private long teacherId;
    private long paymentId;
    private long scheduleId;
    private String teacherPw;
    private Integer paymentPrice;
    private String paymentType;
    private String scheduleClassTime;
    private String startTime;
    private String endTime;
    private String scheduleDate;
    private String month;
    private String dayType;
    private String sumPrice;
    private List<UdTeacher> udTeacherList;

    @Data
    public static class Income {

        private String paymentType;
        List<UdTeacher> udTeacherList;
        List<IncomeMonth> incomeLists;

    }

    @Data
    public static class IncomeMonth {
        private String month;
        private Integer amount;
        List<UdTeacher> udTeacherList;
    }

}
