package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChangeErrorType {

    NONE("NONE"),
    CHILD("CHILD"),
    TEACHER("TEACHER");

    ChangeErrorType(String type) {
    }
}
