package ai.maum.aics.daekyo.ud.web.repository.entity;

import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import ai.maum.aics.daekyo.ud.web.util.ModelMapperUtils;
import lombok.Data;

@Data
public class Schedule {
    private long scheduleId;
    private long serviceId;
    private int scheduleClassTime;
    private int timeBlockIdFrom;
    private int timeBlockIdTo;
    private String scheduleDate;
    private ScheduleType scheduleType;
    private boolean isChange;

    public static Schedule of(ServiceSchedule serviceSchedule) {
        return ModelMapperUtils.getModelMapper().map(serviceSchedule, Schedule.class);
    }
}
