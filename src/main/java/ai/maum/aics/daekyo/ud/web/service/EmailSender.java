package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.EmailDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@RequiredArgsConstructor
@Component
public class EmailSender {
    private final JavaMailSender javaMailSender;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Async("emailThreadExecutor")
    public void send(EmailDto emailDto) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            helper.setTo(emailDto.getTo());
            helper.setText(emailDto.getText(), true);
            helper.setSubject(emailDto.getSubject());

            javaMailSender.send(message);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
