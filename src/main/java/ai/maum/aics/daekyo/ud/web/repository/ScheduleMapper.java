package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.dto.ScheduleDto;
import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import ai.maum.aics.daekyo.ud.web.repository.entity.Alarm;
import ai.maum.aics.daekyo.ud.web.repository.entity.Schedule;
import ai.maum.aics.daekyo.ud.web.repository.entity.ServiceSchedule;
import ai.maum.aics.daekyo.ud.web.repository.entity.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ScheduleMapper {
    int insert(Schedule schedule);
    List<ScheduleDto> getList(long teacherId, List<ScheduleType> scheduleTypeList);
    List<Schedule> getListByDate(long teacherId, String scheduleDate);
    Schedule get(long scheduleId);
    int update(Schedule schedule);
    int complete(long scheduleId);
    List<Long> getScheduleList(long teacherId);
    Teacher getTeacherScheduleAndRequestYn(long teacherId);
    void updateRequestYn(Teacher teacher);
    void updateScheduleYn(Teacher teacher);
    void updateCancel(long scheduleId);
    void insertCancelContent(long scheduleId, String content);
    ServiceSchedule getCancelTypeByScheduleId(long scheduleId);
    Alarm getDetailForAlarm(long serviceId);
    Alarm getDetailForAlarmByScheduleId(long scheduleId);
    int getUnavailableDate(long teacherId, String scheduleDate);
    List<Schedule> getListByChildId(long scheduleId, String scheduleDate);
}
