package ai.maum.aics.daekyo.ud.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	/**
	 * 
	 * @return 20190101
	 */
	public static String getCurrentDate() {
		
		return getCurrentDateTime("yyyyMMdd");
	}
	
	/**
	 * 
	 * @return 20190101235959
	 */
	public static String getCurrentDateTime() {
		
		return getCurrentDateTime("yyyyMMddHHmmss");
	}
	
	/**
	 * 
	 * @param pattern yyyyMMddHHmmss
	 * @return
	 */
	public static String getCurrentDateTime(String pattern) {
		
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		Date date = new Date();
		
		return format.format(date);
	}
	
	/**
	 * 
	 * @param strDate '20200226'
	 * @param ymd Y : Year, M : Month, D : Day
	 * @param value 1, -1 ....
	 * @return
	 */
	public static String addDateString(String strDate, String ymd, int value) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		Date date;
		try {
			date = format.parse(strDate);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			
			if ("Y".equals(ymd)) {
				cal.add(Calendar.YEAR, value);
			}
			else if ("M".equals(ymd)) {
				cal.add(Calendar.MONTH, value);
			}
			else {
				cal.add(Calendar.DATE, value);
			}
			
			date = cal.getTime();
		} catch (ParseException e) {
			return null;
		}
		
		return format.format(date);
	}
}
