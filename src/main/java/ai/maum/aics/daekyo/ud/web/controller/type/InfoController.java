package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/use_information")
@AllArgsConstructor
public class InfoController {

    @GetMapping("/")
    public String noticeList(OAuth2Authentication oAuth2Authentication) {
        return "thymeleaf/use_information";
    }

}
