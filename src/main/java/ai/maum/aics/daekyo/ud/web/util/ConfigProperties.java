package ai.maum.aics.daekyo.ud.web.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "daekyo.server")
public class ConfigProperties {
    private String host;
    private String message;
    private String app;
    private String key;
    private String bookafter;
    private String upload;
    private String authAt;
    private String mailLogo;
    private String mailAlert;
    private String uploadBookafter;
    private String uploadUdong;
}
