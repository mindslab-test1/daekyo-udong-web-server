package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookAfterMapper {
    List<Long> getScheduleIdList(List<Long> scheduleIdList);
    List<Long> getBookcaseIdList(long teacherId, long childId);
    BookafterDto selectReadDateByBookId(BookafterDto dkBookcaseVO);
    void updateReadDate(BookafterDto dkBookcaseVO);
    void registBookafter(BookafterDto dkBookafterVO);
    List<BookAfter> getBookafterList(BookAfter bookafter);
}
