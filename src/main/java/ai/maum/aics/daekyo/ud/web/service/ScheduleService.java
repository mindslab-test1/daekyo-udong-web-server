package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.ScheduleDto;
import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDateDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherTimeDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.VisitTimetableDto;
import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import ai.maum.aics.daekyo.ud.web.repository.*;
import ai.maum.aics.daekyo.ud.web.repository.entity.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ScheduleService {
    private TimeBlockMapper timeBlockMapper;
    private TeacherTimeMapper teacherTimeMapper;
    private TeacherDateMapper teacherDateMapper;
    private ScheduleMapper scheduleMapper;

    @Transactional(readOnly = true)
    public VisitTimetableDto getVisitTimetable(long teacherId) {
        List<TimeBlock> timeBlockList = timeBlockMapper.getTimeBlockList();
        List<TeacherTime> teacherTimeList = teacherTimeMapper.getList(teacherId);
        List<String> teacherUnavailableDate = teacherDateMapper.getList(teacherId);
        List<ScheduleDto> scheduleDtoList = scheduleMapper.getList(teacherId, ScheduleType.getMatchedScheduleTypeList());
        return VisitTimetableDto.of(timeBlockList, teacherTimeList, teacherUnavailableDate, scheduleDtoList);
    }

    @Transactional(readOnly = true)
    public List<TimeBlock> getTimeBlockList() {
        return timeBlockMapper.getTimeBlockList();
    }

    public String getTodayDayWeek() {
        Calendar today = Calendar.getInstance();
        DayType todayDayWeek = DayType.values()[today.get(Calendar.DAY_OF_WEEK)];
        return todayDayWeek.name();
    }

    @Transactional(readOnly = true)
    public TeacherTimeDto.Response getTeacherTimeList(TeacherTimeDto.Request request) {
        TeacherTimeDto.Response response = new TeacherTimeDto.Response();
        response.setTimeBlockList(
                teacherTimeMapper.getListByDayIdx(
                        request.getTeacherId(),
                        request.getDayType()
                )
        );
        return response;
    }

    @Transactional
    public boolean updateTeacherTimeList(TeacherTimeDto.Update update) {
        teacherTimeMapper.delete(
                update.getTeacherId(),
                update.getDayType()
        );

        List<TeacherTime> teacherTimeList = TeacherTimeDto.Update.toEntityList(update);
        if (0 < teacherTimeList.size()) {
            teacherTimeMapper.insertList(teacherTimeList);
        }
        return true;
    }

    @Transactional(readOnly = true)
    public TeacherDateDto.ResponseList getTeacherDateList(TeacherDateDto.RequestList requestList) {
        TeacherDateDto.ResponseList responseList = new TeacherDateDto.ResponseList();
        List<String> dateList = teacherDateMapper.getListByMonth(requestList.getTeacherId(), requestList.getYearMonth())
                .stream()
                .map(s -> String.format(
                        "%s-%s-%s",
                        s.substring(0, 4),
                        s.substring(4, 6),
                        s.substring(6, 8)
                        )
                )
                .collect(Collectors.toList());
        responseList.setDateList(dateList);
        return responseList;
    }

    @Transactional
    public boolean updateTeacherDateList(TeacherDateDto.Update update) {
        teacherDateMapper.delete(update.getTeacherId(), update.getYearMonth());

        for (String date : update.getDateList()) {
            date = date.replaceAll("-", "");
            teacherDateMapper.insert(update.getTeacherId(), date);
        }
        return true;
    }

    @Transactional(readOnly = true)
    public TeacherDateDto.ResponseCheck checkDate(TeacherDateDto.RequestCheck requestCheck) {
        List<Schedule> scheduleList = scheduleMapper.getListByDate(requestCheck.getTeacherId(), requestCheck.getVisitDate());
        TeacherDateDto.ResponseCheck responseCheck = new TeacherDateDto.ResponseCheck();
        boolean result = (scheduleList.size() == 0);
        responseCheck.setResult(result);
        return responseCheck;
    }

    public List<Long> getScheduleList(long teacherId) {
        return scheduleMapper.getScheduleList(teacherId);
    }

    public Teacher getTeacherScheduleAndRequestYn(long teacherId) {
        return scheduleMapper.getTeacherScheduleAndRequestYn(teacherId);
    }

    public void updateRequestYn(Teacher teacher) {
        scheduleMapper.updateRequestYn(teacher);
    }

    public void updateScheduleYn(Teacher teacher) {
        scheduleMapper.updateScheduleYn(teacher);
    }

    public Alarm getDetailForAlarm(long serviceId) {
        return scheduleMapper.getDetailForAlarm(serviceId);
    }
    public Alarm getDetailForAlarmByScheduleId(long scheduleId) {
        return scheduleMapper.getDetailForAlarmByScheduleId(scheduleId);
    }
}
