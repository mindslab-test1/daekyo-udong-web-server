package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.EmailDto;
import ai.maum.aics.daekyo.ud.web.repository.UserMapper;
import ai.maum.aics.daekyo.ud.web.repository.entity.User;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserMapper userMapper;
    private final SpringTemplateEngine templateEngine;
    private final ConfigProperties configProperties;
    private final EmailSender emailSender;

    public Long findCompanyNoForEmail(String name, String email) {
        return userMapper.findCompanyNoForEmail(name, email);
    }

    public Long findDetailForEmail(User user) {
        return userMapper.findDetailForEmail(user.getName(), user.getEmail(), user.getCode());
    }

    public void findId(String email, Long id) {

        Context context = new Context();
        context.setVariable("logo", configProperties.getMailLogo());
        context.setVariable("id", id);
        String html = templateEngine.process("thymeleaf/email_srch_id", context);

        EmailDto emailDto = new EmailDto();
        emailDto.setTo(email);
        emailDto.setSubject("아이디 찾기");
        emailDto.setText(html);

        // Async Call
        emailSender.send(emailDto);
    }

    public void forgotPassword(User user) {

//        todo reset token 포함되게 url을 생성
        String url = String.format("%s/user/update_password?token=&email=%s", configProperties.getHost(), user.getEmail());

        Context context = new Context();
        context.setVariable("logo", configProperties.getMailLogo());
        context.setVariable("url", url);
        String html = templateEngine.process("thymeleaf/email_srch_pw", context);

        EmailDto emailDto = new EmailDto();
        emailDto.setTo(user.getEmail());
        emailDto.setSubject("비밀번호 찾기");
        emailDto.setText(html);

        // Async Call
        emailSender.send(emailDto);
    }
}
