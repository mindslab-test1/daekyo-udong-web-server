package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import ai.maum.aics.daekyo.ud.web.repository.entity.UdTeacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UdTeacherMapper {

    public void updateTeacheePw(UdTeacher udTeacher);
    public List<UdTeacher> getMonthIncomeByTeacherId(UdTeacher udTeacher);
    public List<UdTeacher> getPaymentGroupByTypeAndDate(UdTeacher udTeacher);
    public String getPaymentMinDate(long teacherId);

}
