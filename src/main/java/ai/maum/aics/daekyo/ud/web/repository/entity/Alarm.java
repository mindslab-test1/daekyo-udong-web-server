package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Alarm {
    private long serviceId;
    private long scheduleId;
    private long userId;
    private String addrAddress;
    private String phoneNumber;
    private String teacherName;
    private String teacherTel;
    private String realName;
    private String scheduleTime;
}
