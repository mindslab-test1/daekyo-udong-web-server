package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookShelfDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import ai.maum.aics.daekyo.ud.web.service.BookShelfService;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import ai.maum.aics.daekyo.ud.web.util.DateUtils;
import ai.maum.aics.daekyo.ud.web.util.HttpConnectionUtil;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/book_shelf")
@AllArgsConstructor
public class BookShelfController {
    private BookShelfService bookShelfService;
    private TeacherDetailService teacherDetailService;
    private ConfigProperties configProperties;

    @GetMapping("/view/{scheduleId}")
    public String view(@PathVariable("scheduleId") long scheduleId, BookShelfDto.RequestList requestList, Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));
        model.addAttribute("bookList", bookShelfService.getList(teacherDetailDto.getId(), scheduleId, requestList));
        model.addAttribute("scheduleId", scheduleId);
        return "thymeleaf/bookshelf_view";
    }

    @PostMapping("/add/bookafter")
    public @ResponseBody boolean addBookafter(BookafterDto bookafterDto, OAuth2Authentication oAuth2Authentication) throws Exception {

        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        if (bookafterDto.getImage() != null) {
            int index = bookafterDto.getImage().getOriginalFilename().indexOf(".");
            bookafterDto.setBookafterOrgImage(bookafterDto.getImage().getOriginalFilename().substring(0, index));
            bookafterDto.setBookafterImage(new HttpConnectionUtil().mediaServerSender(bookafterDto.getImage(), configProperties.getUpload(), configProperties.getUploadBookafter()));
        }
        if (bookafterDto.getAudio() != null) {
            int index = bookafterDto.getAudio().getOriginalFilename().indexOf(".");
            bookafterDto.setBookafterOrgAudio(bookafterDto.getAudio().getOriginalFilename().substring(0, index));
            bookafterDto.setBookafterAudio(new HttpConnectionUtil().mediaServerSender(bookafterDto.getAudio(), configProperties.getUpload(), configProperties.getUploadBookafter()));
        }

        if (bookShelfService.selectReadDateByBookId(bookafterDto) == null) {
            bookafterDto.setReadDate(DateUtils.getCurrentDateTime("yyyyMMdd"));
            bookShelfService.updateReadDate(bookafterDto);
        }

        bookafterDto.setTeacherId(teacherDetailDto.getId());

        bookShelfService.registBookafter(bookafterDto);

        return true;
    }

    @GetMapping("/bookafter/{bookcaseId}")
    public String boofafterView(@PathVariable("bookcaseId") long bookcaseId, Model model, OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        BookAfter bookafter = new BookAfter();
        bookafter.setTeacherId(teacherDetailDto.getId());
        bookafter.setBookcaseId(bookcaseId);

        model.addAttribute("bookafterList", bookShelfService.getBookafterList(bookafter));

        return "thymeleaf/bookshelf_after";
    }
}
