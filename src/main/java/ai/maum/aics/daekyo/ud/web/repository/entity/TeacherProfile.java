package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class TeacherProfile {

    private long teacherId;
    private long mailAuthId;
    private String teacherName;
    private String teacherImageUrl;
    private String teacherReadingYn;
    private String teacherCoachingYn;
    private String classTime;
    private String reviewRate;
    private String reviewCnt;
    private String teacherBirth;
    private String teacherTel;
    private String teacherEmail;
    private String teacherGender;
    private String teacherContact;
    private String teacherAddress;
    private String teacherIntroduce;

    private String certificate;
    private String school;
    private String major;
    private String educateLevel;
    private String addrBcode;
    private String addrDetail;
    private String grpId;
    private String cdId;
    private String companyName;
    private String career;

    private long timeBlockId;
    private long dayId;

    private long authId;
    private String authNo;
    private String phoneNumber;

    private String endDate;
    private String email;

    private long serviceId;
    private String serviceClassTime;
    private String gender;
    private String birthDay;
    private String visitLogContent;
    private String reviewContent;

    private String teacherActiveC;
    private String teacherActiveT;
}
