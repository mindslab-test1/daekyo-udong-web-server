package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MailDto {
    private String toAddress;
    private String subject;
    private String text;
}
