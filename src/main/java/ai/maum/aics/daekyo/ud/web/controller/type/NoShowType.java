package ai.maum.aics.daekyo.ud.web.controller.type;

public enum NoShowType {
    CLOSE,
    OPEN,
    IN_PROGRESS
}
