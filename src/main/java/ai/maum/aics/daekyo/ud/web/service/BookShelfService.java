package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.BookShelfDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.BookafterDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.DkMediaResponseDto;
import ai.maum.aics.daekyo.ud.web.repository.*;
import ai.maum.aics.daekyo.ud.web.repository.entity.*;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import lombok.AllArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class BookShelfService {
    private ScheduleMapper scheduleMapper;
    private ServiceMapper serviceMapper;
    private ChildMapper childMapper;
    private BookMapper bookMapper;
    private BookAfterMapper bookAfterMapper;
    private BookAfterDetailMapper bookAfterDetailMapper;
    private ConfigProperties configProperties;

    public BookShelfDto.ResponseList getList(long teacherId, long scheduleId, BookShelfDto.RequestList requestList) {
//        todo 접근 권한 체크

        Schedule schedule = scheduleMapper.get(scheduleId);
        long serviceId = schedule.getServiceId();

        ServiceSchedule serviceSchedule = serviceMapper.getSchedule(serviceId);
        long userId = serviceSchedule.getUserId();
        long childId = serviceSchedule.getChildId();

        String childName = childMapper.getName(childId);

        List<Book> bookList = bookMapper.getBookList(userId, childId);

        List<BookDetail> bookDetailList = bookMapper.getBookDetailList(childId);

        List<Long> bookcaseIdList = bookAfterMapper.getBookcaseIdList(teacherId, childId);

        return BookShelfDto.ResponseList.of(childName, bookList, bookDetailList, bookcaseIdList);
    }

    public BookafterDto selectReadDateByBookId(BookafterDto bookafterDto) {
        return bookAfterMapper.selectReadDateByBookId(bookafterDto);
    }
    public void updateReadDate(BookafterDto dkBookcaseVO) {
        bookAfterMapper.updateReadDate(dkBookcaseVO);
    }

    public void registBookafter(BookafterDto dkBookafterVO) {
        bookAfterMapper.registBookafter(dkBookafterVO);
    }

    public List<BookAfter> getBookafterList(BookAfter bookafter) {
        return bookAfterMapper.getBookafterList(bookafter);
    }
}
