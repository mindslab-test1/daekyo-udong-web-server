package ai.maum.aics.daekyo.ud.web.service;

import ai.maum.aics.daekyo.ud.web.controller.dto.EmailDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.UserDto;
import ai.maum.aics.daekyo.ud.web.repository.TeacherDetailMapper;
import ai.maum.aics.daekyo.ud.web.repository.TeacherProfileMapper;
import ai.maum.aics.daekyo.ud.web.repository.TimeBlockMapper;
import ai.maum.aics.daekyo.ud.web.repository.entity.*;
import ai.maum.aics.daekyo.ud.web.util.ConfigProperties;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@AllArgsConstructor
public class TeacherDetailService {

    private TeacherDetailMapper teacherDetailMapper;
    private TeacherProfileMapper teacherProfileMapper;
    private TimeBlockMapper timeBlockMapper;
    private final SpringTemplateEngine templateEngine;
    private EmailSender emailSender;
    private ConfigProperties configProperties;
    private PasswordEncoder passwordEncoder;

    public TeacherDetailDto selectTeacherByCompanyNo(Long companyNo) {
        return teacherDetailMapper.selectTeacherByCompanyNo(companyNo);
    }

    public TeacherDetailDto selectTeacherByEmail(String email) {
        return teacherDetailMapper.selectTeacherByEmail(email);
    }

    public UdTeacher updateNewPassword(UserDto.UpdatePassword user, Long companyNo) {
        TeacherDetailDto teacherDetailDto;
        if (companyNo == null) {
            teacherDetailDto = teacherDetailMapper.selectTeacherByEmail(user.getEmail());
        } else {
            teacherDetailDto = teacherDetailMapper.selectTeacherByCompanyNo(companyNo);
        }
        if (!user.confirmPassword()) {
            return null;
        }

        String newPasswordHashed = passwordEncoder.encode(user.getPassword());

        UdTeacher udTeacher = new UdTeacher();
        udTeacher.setTeacherId(teacherDetailDto.getId());
        udTeacher.setTeacherPw(newPasswordHashed);

        return udTeacher;
    }

    public TeacherProfile getTeacherDetail(long teacherId) {
        return teacherProfileMapper.getTeacherDetail(teacherId);
    }

    public TeacherProfile getTeacherSchool(long teacherId) {
        return teacherProfileMapper.getTeacherSchool(teacherId);
    }

    public List<TeacherProfile> getTeacherCertificateList(long teacherId) {
        return teacherProfileMapper.getTeacherCertificateList(teacherId);
    }

    public List<TeacherProfile> getTeacherForeignCertificateList(long teacherId) {
        return teacherProfileMapper.getTeacherForeignCertificateList(teacherId);
    }

    public List<TeacherProfile> getTeacherHopeAddrList(long teacherId) {
        return teacherProfileMapper.getTeacherHopeAddrList(teacherId);
    }

    public List<TeacherProfile> getTeacherActivityList(long teacherId) {
        return teacherProfileMapper.getTeacherActivityList(teacherId);
    }
    public void insertActivity(TeacherProfile teacherProfile) throws Exception{
        teacherProfileMapper.insertActivity(teacherProfile);
    }
    public void deleteActivityByTeacherId(long teacherId) throws Exception{
        teacherProfileMapper.deleteActivityByTeacherId(teacherId);
    }

    public List<TeacherTimeBlock> getTimeBlockList(long teacherId) {
        List<TimeBlock> timeBlockList = timeBlockMapper.getTimeBlockList();
        List<TeacherProfile> teacherProfileList = teacherProfileMapper.getTimeBlockByTeacherId(teacherId);
        List<TeacherTimeBlock> teacherTimeBlockList = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            int dayType = 0;
            String day = null;
            switch (i) {
                case 0 : dayType = 2;
                day = "월";
                break;
                case 1 : dayType = 3;
                day = "화";
                break;
                case 2 : dayType = 4;
                day = "수";
                break;
                case 3 : dayType = 5;
                day = "목";
                break;
                case 4 : dayType = 6;
                day = "금";
                break;
                case 5 : dayType = 7;
                day = "토";
                break;
                case 6 : dayType = 1;
                day = "일";
                break;
            }
            List<TimeBlockAvailable> timeBlockAvailableList = new ArrayList<>();
            TeacherTimeBlock teacherTimeBlock = new TeacherTimeBlock();
            teacherTimeBlock.setDayType(dayType);
            teacherTimeBlock.setDay(day);
            for (TimeBlock timeBlock : timeBlockList) {
                TimeBlockAvailable timeBlockAvailable = new TimeBlockAvailable();
                timeBlockAvailable.setTimeBlockId(timeBlock.getId());
                for (TeacherProfile teacherProfile : teacherProfileList) {
                    if (teacherProfile.getDayId() == teacherTimeBlock.getDayType() && teacherProfile.getTimeBlockId() == timeBlockAvailable.getTimeBlockId()) {
                        timeBlockAvailable.setAvailable(true);
                    }
                }
                timeBlockAvailableList.add(timeBlockAvailable);
            }
            teacherTimeBlock.setTimeBlockList(timeBlockAvailableList);
            teacherTimeBlockList.add(teacherTimeBlock);

        }

        return teacherTimeBlockList;
    }

    public List<TeacherProfile> getCareer(long teacherId) {
        return teacherProfileMapper.getCareer(teacherId);
    }

    public MessageVO cellPhoneMessage(TeacherDetailDto teacherDetailDto) {

        Random random = new Random();
        String authNumber = "";
        int authLength = 6;
        for (int i = 0; i < authLength; i++) {
            String ran = Integer.toString(random.nextInt(10));
            authNumber += ran;
        }

        String phoneNumber = teacherDetailDto.getTel();

        MessageVO messageVO = new MessageVO();
        messageVO.setDestPhone(phoneNumber);
        messageVO.setAuthNumber(authNumber);

        TeacherProfile teacherProfile = new TeacherProfile();
        teacherProfile.setAuthNo(authNumber);
        teacherProfile.setTeacherId(teacherDetailDto.getId());
        teacherProfile.setPhoneNumber(phoneNumber);

        teacherProfileMapper.insertAuthByTeacherId(teacherProfile);

        return messageVO;

    }

    public TeacherProfile getPhoneAuthByTeacherId(TeacherProfile teacherProfile) {
        return teacherProfileMapper.getPhoneAuthByTeacherId(teacherProfile);
    }

    public void mailModify(String email, TeacherDetailDto teacherDetailDto) {

        Context context = new Context();
        context.setVariable("logo", configProperties.getMailLogo());
        context.setVariable("href", configProperties.getHost() + configProperties.getMailAlert());
        context.setVariable("id", teacherDetailDto.getId());
        String html = templateEngine.process("thymeleaf/email_modify", context);

        EmailDto emailDto = new EmailDto();
        emailDto.setTo(email);
        emailDto.setSubject("대교 우동선생님 이메일 변경");
        emailDto.setText(html);

        TeacherProfile teacherProfile = new TeacherProfile();
        teacherProfile.setTeacherId(teacherDetailDto.getId());
        teacherProfile.setEmail(email);
        teacherProfileMapper.registMailAuth(teacherProfile);

        // Async Call
        emailSender.send(emailDto);
    }

    public void mailModify1(String email) {

        Context context = new Context();
        context.setVariable("logo", configProperties.getMailLogo());
        context.setVariable("href", configProperties.getHost() + configProperties.getMailAlert());
        context.setVariable("id", 1);
        String html = templateEngine.process("thymeleaf/email_modify", context);

        EmailDto emailDto = new EmailDto();
        emailDto.setTo(email);
        emailDto.setSubject("대교 우동선생님 이메일 변경");
        emailDto.setText(html);

        TeacherProfile teacherProfile = new TeacherProfile();
        teacherProfile.setTeacherId(1);
        teacherProfile.setEmail(email);
        teacherProfileMapper.registMailAuth(teacherProfile);

        // Async Call
        emailSender.send(emailDto);
    }


    public TeacherProfile getMailAuthByTeacherId(long teacherId) {
        return teacherProfileMapper.getMailAuthByTeacherId(teacherId);
    }

    public TeacherProfile getMailAuthByComplete(long teacherId) {
        return teacherProfileMapper.getMailAuthByComplete(teacherId);
    }

    public void updateMailAuthCompleteYn(long mailAuthId) {
        teacherProfileMapper.updateMailAuthCompleteYn(mailAuthId);
    }

    public List<Addr> getAddrSido() {
        return teacherProfileMapper.getAddrSido();
    }
    public List<Addr> getAddrSigungu(Addr addr) {
        return teacherProfileMapper.getAddrSigungu(addr);
    }
    public List<Addr> getAddrBname1(Addr addr) {
        return teacherProfileMapper.getAddrBname1(addr);
    }
    public List<Addr> getAddrBname2(Addr addr) {
        return teacherProfileMapper.getAddrBname2(addr);
    }

    public void updateTeacherDetail(TeacherUpdate teacherUpdate) {
        teacherProfileMapper.updateTeacherDetail(teacherUpdate);
    }

    public String getBcode(Addr addr) {
        return teacherProfileMapper.getBcode(addr);
    }

    public void insertHopeAddr(Addr addr) {
        teacherProfileMapper.insertHopeAddr(addr);
    }
    public void updateHopeAddrDelete(Addr addr) {
        teacherProfileMapper.updateHopeAddrDelete(addr);
    }
    public List<TeacherProfile> getvisitLog(long teacherId) {
        return teacherProfileMapper.getvisitLog(teacherId);
    }

    public void updateTeacherPreferredAge(TeacherProfile teacherProfile) {
        teacherProfileMapper.updateTeacherPreferredAge(teacherProfile);
    }
}
