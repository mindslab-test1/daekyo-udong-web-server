package ai.maum.aics.daekyo.ud.web.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/notice")
@AllArgsConstructor
public class NoticeController {

    @GetMapping("/list")
    public String noticeList(OAuth2Authentication oAuth2Authentication) {
        return "thymeleaf/notice";
    }

    @GetMapping("/list/{noticeId}")
    public String noticeDetail(@PathVariable("noticeId") long noticeId, OAuth2Authentication oAuth2Authentication) {
        return "thymeleaf/notice_view";
    }
}
