package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import ai.maum.aics.daekyo.ud.web.controller.type.VisitType;
import ai.maum.aics.daekyo.ud.web.repository.entity.TeacherTime;
import ai.maum.aics.daekyo.ud.web.repository.entity.TimeBlock;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class VisitTimetableDto {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private List<String> timeList;
    private List<String> monthList;
    private List<DateDto> dateList;

    @Getter
    @Setter
    public static class DateDto {
        private int day;
        private DayType dayType;
        private List<TimeBlockDto> timeBlockList;
    }

    @Getter
    @Setter
    public static class TimeBlockDto {
        private boolean available;
        private VisitDto visit;
    }

    @Getter
    @Setter
    public static class VisitDto {
        private VisitType visitType;
        private int timeBlockIdxFrom;
        private int timeBlockIdxTo;
        private String userName;

        public static VisitDto of(ScheduleDto scheduleDto) {
            VisitDto visitDto = new VisitDto();
            visitDto.setUserName(scheduleDto.getChild().getName());
            visitDto.setTimeBlockIdxFrom(scheduleDto.getDateTime().getStartTimeIdx());
            visitDto.setTimeBlockIdxTo(scheduleDto.getDateTime().getEndTimeIdx());
            visitDto.setVisitType(scheduleDto.getVisitType());
            return visitDto;
        }

        public int getHeight() {
            return 24 * (timeBlockIdxTo - timeBlockIdxFrom + 1);
        }

        public String getName() {
            if (visitType.getKorean().isEmpty()) {
                return userName;
            } else {
                return userName + "<br>" + visitType.getKorean();
            }
        }
    }

    public static VisitTimetableDto of(List<TimeBlock> timeBlockList, List<TeacherTime> teacherTimeList,
                                       List<String> teacherUnavailableDate, List<ScheduleDto> scheduleDtoList) {
        VisitTimetableDto visitTimetableDto = new VisitTimetableDto();

        List<String> timeList = timeBlockList.stream()
                .map(TimeBlock::getStartTime)
                .collect(Collectors.toList());
        TimeBlock lastTimeBlock = timeBlockList.get(timeBlockList.size() - 1);
        timeList.add(lastTimeBlock.getEndTime());
        visitTimetableDto.setTimeList(timeList);

        Map<DayType, List<Integer>> teacherTimeMap = teacherTimeList.stream()
                .collect(
                        Collectors.groupingBy(
                                TeacherTime::getDayType,
                                Collectors.mapping(TeacherTime::getTimeBlockIdx, Collectors.toList())
                        )
                );

        Map<String, List<ScheduleDto>> scheduleDtoMap = scheduleDtoList.stream()
                .collect(
                        Collectors.groupingBy(
                                scheduleDto -> scheduleDto.getDateTime().getDate(),
                                Collectors.mapping(scheduleDto -> scheduleDto, Collectors.toList())
                        )
                );

        Calendar calendarDate = Calendar.getInstance();
        List<String> monthList = new ArrayList<>();
        List<DateDto> dateList = new ArrayList<>();
//        todo 93을 설정에서 가져옴
        for (int dateIdx = 0; dateIdx < 93; dateIdx++) {
            if (dateIdx % 3 == 0) {
                int month = calendarDate.get(Calendar.MONTH) + 1;
                monthList.add(Integer.toString(month));
            }
            DateDto date = new DateDto();
            int day = calendarDate.get(Calendar.DAY_OF_MONTH);
            date.setDay(day);
            DayType dayType = DayType.values()[calendarDate.get(Calendar.DAY_OF_WEEK)];
            date.setDayType(dayType);

            String strDate = sdf.format(calendarDate.getTime());
            List<TimeBlockDto> visitTimeList = new ArrayList<>();
            if (teacherUnavailableDate.contains(strDate) || !teacherTimeMap.containsKey(dayType)) {
                for (int timeIdx = 1; timeIdx <= timeBlockList.size(); timeIdx++) {
                    TimeBlockDto time = new TimeBlockDto();
                    time.setAvailable(false);
                    visitTimeList.add(time);

                }
            } else {
                for (int timeIdx = 1; timeIdx <= timeBlockList.size(); timeIdx++) {
                    boolean timeAvailable = teacherTimeMap.get(dayType).contains(timeIdx);
                    TimeBlockDto time = new TimeBlockDto();
                    time.setAvailable(timeAvailable);
                    visitTimeList.add(time);
                }
            }
            if (scheduleDtoMap.containsKey(strDate)) {
                List<ScheduleDto> scheduleDtos = scheduleDtoMap.get(strDate);
                for (ScheduleDto scheduleDto : scheduleDtos) {
                    int timeBlockIdx = scheduleDto.getDateTime().getStartTimeIdx() - 1;
                    VisitDto visitDto = VisitDto.of(scheduleDto);
                    visitTimeList.get(timeBlockIdx).setVisit(visitDto);
                }
            }
            date.setTimeBlockList(visitTimeList);
            dateList.add(date);
            calendarDate.add(Calendar.DATE, 1);
        }
        visitTimetableDto.setMonthList(monthList);
        visitTimetableDto.setDateList(dateList);
        return visitTimetableDto;
    }
}
