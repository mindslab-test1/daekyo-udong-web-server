package ai.maum.aics.daekyo.ud.web.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHandler {
    private static DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    public static Calendar parse(String dateString) throws ParseException {
        Date date = dateFormat.parse(dateString);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static String toString(Calendar calendar) {
        return dateFormat.format(calendar.getTime());
    }
}
