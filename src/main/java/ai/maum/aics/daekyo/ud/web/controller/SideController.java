package ai.maum.aics.daekyo.ud.web.controller;

import ai.maum.aics.daekyo.ud.web.controller.dto.TeacherDetailDto;
import ai.maum.aics.daekyo.ud.web.controller.dto.UserDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.Teacher;
import ai.maum.aics.daekyo.ud.web.repository.entity.UdTeacher;
import ai.maum.aics.daekyo.ud.web.repository.entity.User;
import ai.maum.aics.daekyo.ud.web.service.ScheduleService;
import ai.maum.aics.daekyo.ud.web.service.TeacherDetailService;
import ai.maum.aics.daekyo.ud.web.service.UdTeacherService;
import ai.maum.aics.daekyo.ud.web.service.UserService;
import ai.maum.aics.daekyo.ud.web.util.OAuth2AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/side")
@AllArgsConstructor
public class SideController {

    private TeacherDetailService teacherDetailService;
    private ScheduleService scheduleService;

    @PostMapping("/scheduled")
    public @ResponseBody boolean findId(OAuth2Authentication oAuth2Authentication) {
        String companyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        TeacherDetailDto teacherDetailDto = teacherDetailService.selectTeacherByCompanyNo(Long.parseLong(companyNo));

        Teacher teacher = scheduleService.getTeacherScheduleAndRequestYn(teacherDetailDto.getId());

        if (teacher.getScheduleYn().equals("Y")) {
            return true;
        } else {
            return false;
        }
    }

}
