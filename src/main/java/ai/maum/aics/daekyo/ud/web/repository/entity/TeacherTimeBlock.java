package ai.maum.aics.daekyo.ud.web.repository.entity;

import ai.maum.aics.daekyo.ud.web.controller.type.DayType;
import lombok.Data;

import java.util.List;

@Data
public class TeacherTimeBlock {
    private long dayType;
    private String day;
    private List<TimeBlockAvailable> timeBlockList;

}
