package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class TeacherDateDto {

    @Getter
    @Setter
    public static class RequestList {
        private long teacherId;
        private String yearMonth;

        public String getYearMonth() {
            return yearMonth.replaceAll("-", "");
        }
    }

    @Getter
    @Setter
    public static class ResponseList {
        private List<String> dateList;
    }

    @Getter
    @Setter
    public static class Update {
        private long teacherId;
        private String yearMonth;
        private List<String> dateList;

        public String getYearMonth() {
            return yearMonth.replaceAll("-", "");
        }
    }

    @Getter
    @Setter
    public static class RequestCheck {
        private long teacherId;
        private String visitDate;

        public String getVisitDate() {
            return visitDate.replaceAll("-", "");
        }
    }

    @Getter
    @Setter
    public static class ResponseCheck {
        private boolean result;
    }
}
