package ai.maum.aics.daekyo.ud.web.custom;

public class NotEqualException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public NotEqualException(String message) {
        super("not equal", message);
        this.message = message;
    }


}
