package ai.maum.aics.daekyo.ud.web.custom;

public class TypeMissMatchException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public TypeMissMatchException(String message) {
        super("type miss match exception", message);
        this.message = message;
    }


}
