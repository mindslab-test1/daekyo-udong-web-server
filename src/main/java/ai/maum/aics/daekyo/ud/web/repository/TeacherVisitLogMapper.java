package ai.maum.aics.daekyo.ud.web.repository;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherVisitLogMapper {
    int insert(long scheduleId, String visitLogContent);
    List<Long> getScheduleIdList(List<Long> scheduleIdList);
}
