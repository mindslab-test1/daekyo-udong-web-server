package ai.maum.aics.daekyo.ud.web.repository;

import ai.maum.aics.daekyo.ud.web.controller.dto.ApplyDto;
import ai.maum.aics.daekyo.ud.web.repository.entity.Apply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ApplyMapper {
    List<ApplyDto> getApplyList(long teacherId);
    int match(long serviceId);
    int reject(long applyId);
    Apply get(long applyId);
}
