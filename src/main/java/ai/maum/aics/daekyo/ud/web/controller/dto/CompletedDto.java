package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.controller.type.ScheduleType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CompletedDto {

    @Getter
    @Setter
    public static class WriteVisitLog {
        private long teacherId;
        private long scheduleId;
        @NotEmpty
        @Size(min=200)
        private String content;
    }

    @Getter
    @Setter
    public static class Get {
        private ScheduleDto scheduleDto;
        private String review;
        private long bookAfterCnt;
        private boolean hasVisitLog;

        public boolean isVisitLogActive() {
            return scheduleDto.getScheduleType() == ScheduleType.COMPLETED && 0 < bookAfterCnt && !hasVisitLog;
        }

        public boolean isBookAfterActive() {
            return bookAfterCnt == 0 && !hasVisitLog;
        }

        public boolean isBookShelfActive() {
            return !hasVisitLog;
        }
    }

    @Getter
    @Setter
    public static class TeacherCancel {
        private long teacherId;
        private long scheduleId;
        private String content;
    }
}
