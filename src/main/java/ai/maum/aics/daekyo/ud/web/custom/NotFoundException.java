package ai.maum.aics.daekyo.ud.web.custom;

public class NotFoundException extends BaseException {
    private String message;

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public NotFoundException(String message) {
        super("not found exception", message);
        this.message = message;
    }


}
