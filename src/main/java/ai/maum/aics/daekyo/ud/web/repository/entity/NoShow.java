package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class NoShow {
    private long scheduleId;
    private long noShowId;
}
