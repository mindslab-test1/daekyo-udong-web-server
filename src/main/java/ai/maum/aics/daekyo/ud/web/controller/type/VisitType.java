package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VisitType {
    ONETIME("onetime_visit", ""),
    REGULAR("regular_visit", "정기방문");

    private String cls;
    private String korean;
}
