package ai.maum.aics.daekyo.ud.web.controller.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenderType {
    NONE(""),
    MALE("남아"),
    FEMALE("여아");

    private String korean;
}
