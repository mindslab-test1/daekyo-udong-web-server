package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class BookAfter {

    private long bookafterId;
    private long bookcaseId;
    private long bookId;
    private long userId;
    private long childId;
    private long teacherId;
    private String bookafterTitle;
    private String bookafterContent;
    private String bookafterPrivateYn;
    private String bookafterFavoriteCnt;
    private String bookafterViewCnt;
    private String updateYn;
    private String readDate;
    private String regDate;

    private String realName;

    private MultipartFile image;
    private MultipartFile audio;

    private String bookafterImage;
    private String bookafterAudio;
    private String bookafterOrgImage;
    private String bookafterOrgAudio;

}
