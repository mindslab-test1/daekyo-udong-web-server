package ai.maum.aics.daekyo.ud.web.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/css/**", "/js/**", "/font/**", "/images/**", "/video/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        http.requiresChannel()
//                .anyRequest()
//                .requiresSecure();

        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/**").authenticated();

        http.formLogin()
                .loginPage("/user/login")
                .defaultSuccessUrl("/visit_timetable")
                .permitAll();

        http.logout()
                .logoutSuccessUrl("/user/login")
                .invalidateHttpSession(true);

        http.exceptionHandling().accessDeniedPage("/user/login");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        // prefix algorithm for DelegatingPasswordEncoder
        DelegatingPasswordEncoder delegatingPasswordEncoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        return delegatingPasswordEncoder;
    }
}
