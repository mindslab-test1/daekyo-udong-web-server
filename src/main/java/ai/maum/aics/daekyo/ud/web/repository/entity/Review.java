package ai.maum.aics.daekyo.ud.web.repository.entity;

import lombok.Data;

@Data
public class Review {
    private long scheduleId;
    private String reviewContent;
}
