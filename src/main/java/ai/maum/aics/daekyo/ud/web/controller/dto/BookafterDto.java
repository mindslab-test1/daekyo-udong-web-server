package ai.maum.aics.daekyo.ud.web.controller.dto;

import ai.maum.aics.daekyo.ud.web.repository.entity.BookAfter;
import ai.maum.aics.daekyo.ud.web.util.ModelMapperUtils;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class BookafterDto {

    private long bookafterId;
    private long bookcaseId;
    private long bookId;
    private long userId;
    private long childId;
    private long teacherId;
    private long scheduleId;
    private String bookafterTitle;
    private String bookafterContent;
    private String bookafterPrivateYn;
    private String bookafterFavoriteCnt;
    private String bookafterViewCnt;
    private String updateYn;
    private String readDate;

    private String realName;

    private MultipartFile image;
    private MultipartFile audio;

    private String bookafterImage;
    private String bookafterAudio;
    private String bookafterOrgImage;
    private String bookafterOrgAudio;


}
