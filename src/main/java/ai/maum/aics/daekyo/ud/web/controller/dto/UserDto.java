package ai.maum.aics.daekyo.ud.web.controller.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserDto {

    @Getter
    @Setter
    public static class FindId {
        @NotBlank
        private String name;
        @Email
        private String email;
    }

    @Getter
    @Setter
    public static class ForgotPassword {
        private long companyNo;
        @NotBlank
        private String name;
        @Email
        private String email;
    }

    @Getter
    @Setter
    public static class UpdatePassword {
        @NotBlank
        private String token;
        @Email
        private String email;
        @Pattern(regexp = "^(?=.{8,16}$)(?=.*[a-zA-Z])(?=.*[0-9])(?=.*\\W).*$")
        private String password;
        private String passwordRepeated;

        public boolean confirmPassword() {
            return password == null || password.equals(passwordRepeated);
        }
    }
}
