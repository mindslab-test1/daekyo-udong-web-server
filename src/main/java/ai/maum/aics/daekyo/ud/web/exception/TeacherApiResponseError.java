package ai.maum.aics.daekyo.ud.web.exception;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class TeacherApiResponseError {
    private int statusCode;
    private String code;
    private String message;
}
