
function checkJwtTokenInCookie() {
    // TODO: refactoring switch range condition..? please check
    if (isAllTokensPresent()) {
        console.log('exist access_token, refresh_token');
    }
    else if (isRefreshTokenPresent()) {
        console.log('not exist access_token');
        if (window.location.pathname === '/user/login') {
            console.log('exist refresh_token');
            $.removeCookie('refresh_token');
            window.location.reload();
            return;
        }
        const data = {
            grant_type: 'refresh_token',
            refresh_token: $.cookie('refresh_token')
        };

        $.ajax({
            async: false,
            type: 'POST',
            url: authUrl,
            headers: {
                'Authorization': 'Basic ' + btoa('daekyo-teacher:daekyo-teacher-secret12345')
            },
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function (data, textStatus, jqXHR) {
            const res = data;

            const decoded_access_token = jwt_decode(res.access_token);

            // set cookie access-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('access_token', res.access_token, {
                expires: moment.unix(decoded_access_token.exp).toDate(),
                path: '/',
                secure: false
            });

            const nowUrl = document.URL;
            const replaceUrl = getUrlParameter(nowUrl);
            window.location.replace(replaceUrl);

        }).fail(function (data) {
            const response = data.responseJSON;
            console.warn(response.error);
            // Resolve P404-340 issue: Remove alert
        });
    }
    else if (isNotRefreshTokenPresent()) {
        console.log('exist access_token but not exist refresh_token');
        if (window.location.pathname !== '/user/login') {
            $.removeCookie('access_token');
            window.location.href = '/user/login';
        }
    }
    else {
        console.log('required login');
        if (window.location.pathname !== '/user/update_password' &&  window.location.pathname !== '/user/login' ) {
            window.location.href = '/user/login';
        }
    }
}

function isAllTokensPresent() {
    return ($.cookie('access_token') && $.cookie('refresh_token'));
}

function isRefreshTokenPresent() {
    return (!$.cookie('access_token') && $.cookie('refresh_token'));
}

function isNotRefreshTokenPresent() {
    return ($.cookie('access_token') && !$.cookie('refresh_token'));
}

function getUrlParameter(nowUrl) {
    let sPageURL = nowUrl.substring(1),
        sURLVariables = sPageURL.split('&'),
        sUrlPage = sURLVariables[0].split('=');

        return decodeURIComponent(sUrlPage[1]);
}

checkJwtTokenInCookie();
