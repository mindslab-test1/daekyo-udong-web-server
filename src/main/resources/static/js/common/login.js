
const auth = {
    url: authUrl,
    authorization: {
        clientId: 'daekyo-teacher',
        clientSecret: 'daekyo-teacher-secret12345'
    },
    grant_type: {
        password: 'password',
        refreshToken: 'refresh_token'
    }
}

const index = {
    init: function () {
        const _this = this;
        $('#btn_login').on('click', function () {
            _this.login();
        });

        $('#logout').on('click', function () {
            _this.logout();
        });
    },

    login: function () {

        const data = {
            grant_type: auth.grant_type.password,
            username: $('#teacher_company').val(),
            password: $('#password').val()
        };

        $.ajax({
            async: false,
            type: 'POST',
            url: authUrl,
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Authorization': 'Basic ' + btoa(auth.authorization.clientId + ":" + auth.authorization.clientSecret)
            },
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function (data) {
            const res = data;
            const decoded_access_token = jwt_decode(res.access_token);

            // set cookie access-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('access_token', res.access_token, {
                expires: moment.unix(decoded_access_token.exp).toDate(),
                path: '/',
                secure: false
            });

            const decoded_refresh_token = jwt_decode(res.refresh_token);

            // set cookie refresh-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('refresh_token', res.refresh_token, {
                expires: moment.unix(decoded_refresh_token.exp).toDate(),
                path: '/',
                secure: false
            });

            window.location.replace('/visit_timetable');

        }).fail(function (data) {
            const response = data.responseJSON;
            console.warn(response.error + ':' + response.error_description);
            $(".user_check").removeClass("hide");
        });

    },

    logout: function () {
        $.removeCookie('access_token');
        $.removeCookie('refresh_token');
        window.location.href = '/';
    },
}

index.init();

